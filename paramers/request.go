package paramers

type LoginReq struct {
	Mobile   int64  `json:"mobile" form:"mobile"`
	Password string `json:"password" form:"password"`
	Email    string `json:"email" form:"email"`
}

type LoginResp struct {
	// Mobile   string `json:"mobile" form:"mobile"`
	UserID int64  `json:"user_id"`
	Token  string `json:"token" form:"token"`
	// Email    string `json:"email" form:"email"`
}

type CountryListResp struct {
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	LangFlag string `json:"lang_flag"`
	Code     int    `json:"code"`
	Lang     string `json:"lang"`
	Money    string `json:"money"`
}

type RegisterReq struct {
	Mobile   int64  `json:"mobile" form:"mobile" binding:"required"`
	Password string `json:"password" form:"password"`
	Email    string `json:"email" form:"email"`
	Country  int64  `gorm:"column:country" json:"country"`
	Username string `gorm:"column:username" json:"username"`
}
