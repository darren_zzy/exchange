package paramers

import "net/http"

const (
	StatusOK               = 0
	ERR_CODE_DISPLAY_ERROR = 1
	ERR_CODE_BAD_REQUEST   = http.StatusBadRequest
	ERR_CODE_NOT_FOUND     = http.StatusNotFound
	ERR_CODE_FORBIDDEN     = http.StatusForbidden
	ERR_CODE_INTERNAL      = http.StatusInternalServerError
)

const (
	ErrLoadCfgMsg = "获取配置信息失败"
	ErrParamMsg   = "参数不合法"
	ErrCalcMsg    = "获取价格信息失败"
)
