package paramers

// order status: 1 创建订单 2 已打款  3确认收到  4未收到，申诉   5结束订单 6完成订单
// 定义订单状态
const (
	ORDER_START    = int64(1) // 订单生成，待付款
	ORDER_SERVICED = int64(2) // 已打款
	// ORDER_SERVICED    = int64(3) // 确认收到
	ORDER_COMPLAINING = int64(4) // 未收到，发起申诉
	ORDER_CLOSED      = int64(5) // 因为申诉，订单已关闭
	ORDER_COMPLETED   = int64(6) // 完成订单
)

// 广告交易状态
const (
	NOT_TREADING = int64(1) // 广告未交易
	IS_TREADING  = int64(2) // 广告交易中
)

// 法币交易类型
const (
	OTC_SELL_TYPE = int64(1) // 法币交易 卖
	OTC_BUY_TYPE  = int64(2) // 法币交易 买
)

// 法币交易类型
const (
	WALLET_ADDRESS_DEPOSIT    = int64(1) // 1 充币地址
	WALLET_ADDRESS_WITHDRAWAL = int64(2) // 2 提币地址
)

// 订单描述文案
var ORDER_STATE = map[int64]string{
	ORDER_START:       "订单生成，待付款",
	ORDER_SERVICED:    "已打款",
	ORDER_COMPLAINING: "未收到，发起申诉",
	ORDER_CLOSED:      "因为申诉，订单已关闭",
	ORDER_COMPLETED:   "完成订单",
}

const (
	CONFIRM_ORDER_TIMEOUT  = 15 // 未确认超时
	COMPLETE_ORDER_TIMEOUT = 15 //  未完成放币超时，自动到申诉
	COMPLAIN_ORDER_TIMEOUT = 15 //
)

// 银行卡认证状态 1 通过认证，2 未认证 3 认证不通过 4 用户关闭
const (
	PROFILE_OK  = 1
	PROFILE_BAN = 2
	PROFILE_OFF = 3
)

// MemberTransaction 钱包流水交易类型:
const (
	RECHARGE          = 1  // "充值"
	WITHDRAW          = 2  // "提现"
	TRANSFER_ACCOUNTS = 3  // "转账"
	EXCHANGE          = 4  // "币币交易"
	OTC_BUY           = 5  // "法币买入"
	OTC_SELL          = 6  // "法币卖出"
	ACTIVITY_AWARD    = 7  // "活动奖励"
	PROMOTION_AWARD   = 8  // "推广奖励"
	DIVIDEND          = 9  // "分红"
	VOTE              = 10 // "投票"
	ADMIN_RECHARGE    = 11 // "人工充值"
	MATCH             = 12 // "配对"
	ACTIVITY_BUY      = 13 // "活动兑换"
	CTC_BUY           = 14 // "CTC 买入"
	CTC_SELL          = 15 // "CTC 卖出"
	RED_OUT           = 16 // "红包发出"
	RED_IN            = 17 // "红包领取"
)
