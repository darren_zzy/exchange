# exchange

## 项目简介

1.目录结构

```
exchange
├─app 
│   ├──user_center  用户中心
│   ├──otc          交易所
│   └──admin        管理后台  
├─paramers          请求参数集
├─pkg               类库
│   ├──context      框架上下文的封装
│   ├──midleware    中间件
│   │    ├──permit  认证
│   │    └──.....    
└readme.md

```