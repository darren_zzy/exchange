module git.digittraders.com/exchange/pkg/middleware/permit

go 1.14

require (
	github.com/go-kratos/kratos v0.5.0
	github.com/pkg/errors v0.9.1
)
