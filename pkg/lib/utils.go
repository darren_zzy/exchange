package lib

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"hash"
	"regexp"
	"strconv"
	"strings"
)

// 生成鉴权签名
func Authorize(secret, method, bucket string, expire int64) string {
	var (
		content   string
		mac       hash.Hash
		signature string
	)
	content = fmt.Sprintf("%s\n%s\n%d\n", method, bucket, expire)
	mac = hmac.New(sha1.New, []byte(secret))
	mac.Write([]byte(content))
	signature = base64.StdEncoding.EncodeToString(mac.Sum(nil))

	return signature
}

// phone verify
func VerifyPhone(num string) bool {
	reg := `^1([3589]\d|4[5-9]|6[124567]|7[0-8])\d{8}$`
	rgx := regexp.MustCompile(reg)
	return rgx.MatchString(num)
}

// email verify
func VerifyEmail(email string) bool {
	pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*` //匹配电子邮箱
	// pattern := `^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$`
	reg := regexp.MustCompile(pattern)
	return reg.MatchString(email)
}

func GetRemoteIp(remote string) string {
	remoteIps := strings.Split(remote, ":")
	if len(remoteIps) < 1 {
		return ""
	}
	return remoteIps[0]

}

// 截取小数位数
func FloatRound(f float64, n int) float64 {
	format := "%." + strconv.Itoa(n) + "f"
	res, _ := strconv.ParseFloat(fmt.Sprintf(format, f), 64)
	return res
}
