package uuid

import (
	"crypto/md5"
	"fmt"
	"hash/crc32"
	"io"
	"os"
	"sync"
)

var once sync.Once
var workerID int64

// WorkerID 根据主机名和自定义的identity生成进程唯一识别码
// identity 建议使用能够区分当前进程在当前主机的唯一性
// 例如：http服务的listen address、process id等
func WorkerID(identity string) int64 {
	once.Do(func() {
		hostname, err := os.Hostname()
		if err != nil {
			hostname = NewV1().Hex()
		}
		if identity == "" {
			identity = fmt.Sprintf("pid%d", os.Getpid())
		}
		h := md5.New()
		io.WriteString(h, hostname)
		io.WriteString(h, identity)
		workerID = int64(crc32.ChecksumIEEE(h.Sum(nil)))
	})
	return workerID
}
