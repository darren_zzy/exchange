// +build wireinject
// The build tag makes sure the stub is not built in the final build.

package di

import (
	"git.digittraders.com/exchange/wallet/internal/dao"
	"git.digittraders.com/exchange/wallet/internal/server/grpc"
	"git.digittraders.com/exchange/wallet/internal/server/http"
	"git.digittraders.com/exchange/wallet/internal/service"
	"github.com/google/wire"
)

//go:generate kratos t wire
func InitApp() (*App, func(), error) {
	panic(wire.Build(dao.Provider, service.Provider, http.New, grpc.New, NewApp))
}
