module git.digittraders.com/exchange

go 1.14

require (
	git.digittraders.com/exchange/paramers v0.0.0
	git.digittraders.com/exchange/pkg/lib v0.0.0
	git.digittraders.com/exchange/wallet v0.0.0
	github.com/go-kratos/kratos v0.5.1-0.20200526160825-521d240568d0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.1
	github.com/google/uuid v1.1.1
	github.com/google/wire v0.4.0
	github.com/gorilla/websocket v1.4.2
	github.com/jinzhu/gorm v1.9.12
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/tools v0.0.0-20200526224456-8b020aee10d2 // indirect
	google.golang.org/genproto v0.0.0-20200511104702-f5ebc3bea380
	google.golang.org/grpc v1.29.1
)

replace git.digittraders.com/exchange/wallet => /Users/darren/go/src/git.digittraders.com/exchange/app/wallet

replace github.com/go-kratos/kratos => /Users/darren/go/src/git.digittraders.com/kratos

replace git.digittraders.com/exchange/pkg/lib => /Users/darren/go/src/git.digittraders.com/exchange/pkg/lib

replace git.digittraders.com/exchange/paramers => /Users/darren/go/src/git.digittraders.com/exchange/paramers