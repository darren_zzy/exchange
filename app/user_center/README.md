# UserCenter

## Requirments
  Go version>=1.13

## 项目简介
1. 用户中心 ： 实现场外交易，撮合订单，用户通知


## 项目启动流程
1. 拉取官方框架到 mod 环境

```
    GO111MODULE=on
    go get github.com/go-kratos/kratos/tool/kratos
``` 

2. 下载项目框架代码 到 $GOPATH/src/git.digittraders.com 下： 
    
```
    GO111MODULE=on
    cd $GOPATH/src/git.digittraders.com
    clone ssh://root@192.168.3.8:23/darren/kratos.git

```
   
### Build:编译框架及工具
   
   ```
   cd $GOPATH/src/git.digittraders.com/kratos/tool
   
   // 主框架
   cd kratos
   go build && mv kratos $GOPATH/bin
     
   // 以下工具链，可快速生成标准项目，或者通过Protobuf生成代码，非常便捷使用gRPC、HTTP、swagger文档；
   // 项目生成工具
   cd kratos-gen-project
   go build && mv kratos-gen-project $GOPATH/bin
   
   // bm工具
   cd protoc-gen-bm
   go build && mv protoc-gen-bm $GOPATH/bin
   
   // 缓存代码生成工具
   cd kratos-gen-bts
   go build && mv kratos-gen-bts $GOPATH/bin
   
   // protoc生成工具
   cd kratos-protoc
   go build && mv kratos-protoc $GOPATH/bin
   
   // 缓存代码生成工具
   cd kratos-gen-mc
   go build && mv kratos-gen-mc $GOPATH/bin
   
   ```
   
   
2. clone 项目代码 exchange  到 $GOPATH/src/git.digittraders.com 后打开 app/user_center目录

#### 运行项目环境 （启用 docker 环境配置 ）：

```
    cd test
    
    // 启动 mysql：13306、 redis：6379、  etcd：2379、 websocket ：888  http:8000 grpc:9000 运行环境 ，请预留出以上端口  
    docker-composer up -d
    
    // 查看环境运行情况
    docker ps 

```

### 运行swagger api 文档 ,请先预留出端口 9900 
```
    cd $GOPATH/src/git.digittraders.com/exchange/pkg/swagger
    
    docker-composer up -d
    
    # 查看环境运行情况
    docker ps |grep swagger

```

### 运行项目
#####  方法一：直接运行：
   ```bash
    kratos run

```

##### 方法二：由docker启动

```
    
    cd test
    // 注意目前运行环境是 linux 启动
    make docker

```

3. 通过proto 生成 grpc， http， websocket

```
    cd api
    ./gen.sh

``` 