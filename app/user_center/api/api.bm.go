// Code generated by protoc-gen-bm v0.1, DO NOT EDIT.
// source: api.proto

/*
Package api is a generated blademaster stub package.
This code was generated with kratos/tool/protobuf/protoc-gen-bm v0.1.

package 命名使用 {appid}.{version} 的方式, version 形如 v1, v2 ..

It is generated from these files:
	api.proto
	message.proto
*/
package api

import (
	"context"

	bm "github.com/go-kratos/kratos/pkg/net/http/blademaster"
	"github.com/go-kratos/kratos/pkg/net/http/blademaster/binding"
)
import google_protobuf1 "github.com/golang/protobuf/ptypes/empty"

// to suppressed 'imported but not used warning'
var _ *bm.Context
var _ context.Context
var _ binding.StructValidator

var PathUserCenterPing = "/exchange/usercenter/ping"
var PathUserCenterCountryList = "/exchange/usercenter/country_list"
var PathUserCenterRegister = "/exchange/usercenter/register"
var PathUserCenterLogin = "/exchange/usercenter/login"
var PathUserCenterAuthPing = "/exchange/usercenter/auth_ping"
var PathUserCenterAdvertiseList = "/exchange/usercenter/advertise_list"
var PathUserCenterSync = "/exchange/usercenter/sync"
var PathUserCenterDelAdvertise = "/exchange/usercenter/del_advertise"
var PathUserCenterAddAdvertise = "/exchange/usercenter/add_advertise"
var PathUserCenterAuthAdvertise = "/exchange/usercenter/auth_advertise"
var PathUserCenterRealIdentity = "/exchange/usercenter/real_identity"
var PathUserCenterAddPayWay = "/exchange/usercenter/add_pay_way"
var PathUserCenterOrderAdvertise = "/exchange/usercenter/order_advertise"
var PathUserCenterOrderAdvertiseQuick = "/exchange/usercenter/order_advertise_quick"
var PathUserCenterComplain = "/exchange/usercenter/complain"
var PathUserCenterConfirm = "/exchange/usercenter/confirm"
var PathUserCenterComplete = "/exchange/usercenter/complete"
var PathUserCenterInternalComplete = "/exchange/usercenter/internal_complete"
var PathUserCenterMyCenter = "/exchange/usercenter/my_center"
var PathUserCenterCurrencyList = "/exchange/usercenter/currency_list"
var PathUserCenterAdvertiseInfo = "/exchange/usercenter/advertise_info"
var PathUserCenterOrderInfo = "/exchange/usercenter/order_info"
var PathUserCenterUpdateMyCenter = "/exchange/usercenter/update_my_center"
var PathUserCenterAuthTread = "/exchange/usercenter/auth_tread"
var PathUserCenterOrderList = "/exchange/usercenter/order_list"
var PathUserCenterGetWithdrawal = "/exchange/usercenter/get_withdrawal"
var PathUserCenterBillList = "/exchange/usercenter/bill_list"
var PathUserCenterDeposit = "/exchange/usercenter/deposit"
var PathUserCenterWithdrawal = "/exchange/usercenter/withdrawal"
var PathUserCenterAddWithdrawal = "/exchange/usercenter/add_withdrawal"
var PathUserCenterAddDeposit = "/exchange/usercenter/add_deposit"
var PathUserCenterAccountBalance = "/exchange/usercenter/account_balance"
var PathUserCenterOrderCancel = "/exchange/usercenter/order_cancel"
var PathUserCenterDealHistory = "/exchange/usercenter/deal_history"
var PathUserCenterGetSwitchAds = "/exchange/usercenter/get_switch_ads"
var PathUserCenterSwitchAds = "/exchange/usercenter/switch_ads"

var PathWalletSync = "/exchange/wallet/sync"
var PathWalletWithdrawalRmb = "/exchange/wallet/withdrawal_rmb"
var PathWalletTransaction = "/exchange/wallet/transaction"
var PathWalletTransfers = "/exchange/wallet/transfers"
var PathWalletTransfersHistory = "/exchange/wallet/transfers_history"
var PathWalletBankList = "/exchange/wallet/bank_list"
var PathWalletDefaultBank = "/exchange/wallet/default_bank"
var PathWalletDelBank = "/exchange/wallet/del_bank"
var PathWalletAccountBalance = "/exchange/wallet/account_balance"
var PathWalletAuthTread = "/exchange/wallet/auth_tread"

// UserCenterBMServer is the server API for UserCenter service.
// 定义UserCenter服务
type UserCenterBMServer interface {
	AuthUser(ctx *bm.Context) (err error)

	Ping(ctx context.Context, req *google_protobuf1.Empty) (resp *google_protobuf1.Empty, err error)

	// 获取国家信息
	// `method:"get" `
	CountryList(ctx context.Context, req *EmptyReq) (resp *CountryListResp, err error)

	// 注册
	// `method:"post" `
	Register(ctx context.Context, req *RegisterReq) (resp *RegisterResp, err error)

	// 登录
	// `method:"post"`
	Login(ctx context.Context, req *LoginReq) (resp *LoginResp, err error)

	// 鉴权后ping
	// `midware:"auth" method:"get"`
	AuthPing(ctx context.Context, req *EmptyReq) (resp *EmptyReq, err error)

	// 广告列表 || 广告发布者列表
	AdvertiseList(ctx context.Context, req *AdvertiseListReq) (resp *AdvertiseListResp, err error)

	// 合并接口
	// `method:"get"`
	Sync(ctx context.Context, req *EmptyReq) (resp *SyncResp, err error)

	// 关闭广告
	// `midware:"auth" method:"post"`
	DelAdvertise(ctx context.Context, req *DelAdvertiseReq) (resp *EmptyReq, err error)

	// 发布广告
	// `midware:"auth" method:"post"`
	AddAdvertise(ctx context.Context, req *AddAdvertiseReq) (resp *EmptyReq, err error)

	// 广告认证权限
	// `midware:"auth" method:"get"`
	AuthAdvertise(ctx context.Context, req *EmptyReq) (resp *EmptyReq, err error)

	// 上传实名制
	// `midware:"auth" method:"post"`
	RealIdentity(ctx context.Context, req *RealIdentityReq) (resp *EmptyReq, err error)

	// 上传支付方式认证
	// `midware:"auth" method:"post"`
	AddPayWay(ctx context.Context, req *AddPayWayReq) (resp *EmptyReq, err error)

	// OTC 广告下单
	//    返回订单id
	// `midware:"auth" method:"post"`
	OrderAdvertise(ctx context.Context, req *OrderAdvertiseReq) (resp *OrderAdvertiseResp, err error)

	// OTC 广告快捷下单
	//    返回订单id
	// `midware:"auth" method:"post"`
	OrderAdvertiseQuick(ctx context.Context, req *OrderAdvertiseReq) (resp *OrderAdvertiseResp, err error)

	// 申诉订单
	// `midware:"auth" method:"post"`
	Complain(ctx context.Context, req *ComplainReq) (resp *EmptyReq, err error)

	// 确认订单已打款
	// `midware:"auth" method:"post"`
	Confirm(ctx context.Context, req *ConfirmReq) (resp *EmptyReq, err error)

	// 完成订单
	// `midware:"auth" method:"post"`
	Complete(ctx context.Context, req *CompleteReq) (resp *EmptyReq, err error)

	//  Internal 完成订单 (内部调用)
	// ` method:"post"`
	InternalComplete(ctx context.Context, req *CompleteReq) (resp *EmptyReq, err error)

	// 个人中心
	// `midware:"auth" method:"get"`
	MyCenter(ctx context.Context, req *EmptyReq) (resp *MyCenterResp, err error)

	// 法币种列表
	// `method:"get"`
	CurrencyList(ctx context.Context, req *CurrencyListReq) (resp *CurrencyListResp, err error)

	// 广告信息
	// `method:"get"`
	AdvertiseInfo(ctx context.Context, req *AdvertiseInfoReq) (resp *Advertise, err error)

	// 订单信息
	// `midware:"auth" method:"get"`
	OrderInfo(ctx context.Context, req *OrderDetailReq) (resp *OrderDetailResp, err error)

	// 修改个人中心数据
	// `midware:"auth" method:"post"`
	UpdateMyCenter(ctx context.Context, req *UpdateMyCenterReq) (resp *EmptyReq, err error)

	// 验证资金密码
	// `midware:"auth" method:"post"`
	AuthTread(ctx context.Context, req *AuthTreadReq) (resp *EmptyReq, err error)

	// 订单列表
	// `midware:"auth" method:"get"`
	OrderList(ctx context.Context, req *AuthTreadReq) (resp *EmptyReq, err error)

	// 获取 币种 地址  二维码,
	// `midware:"auth" method:"get"`
	GetWithdrawal(ctx context.Context, req *GetWithdrawalReq) (resp *GetWithdrawalResp, err error)

	//  充提币 账单记录
	// `midware:"auth" method:"get"`
	BillList(ctx context.Context, req *BillListReq) (resp *BillListResp, err error)

	//  充币
	// `midware:"auth" method:"post"`
	Deposit(ctx context.Context, req *DepositReq) (resp *DepositResp, err error)

	//  提币申请
	// `midware:"auth" method:"post"`
	Withdrawal(ctx context.Context, req *WithdrawalReq) (resp *WithdrawalResp, err error)

	//  添加提币地址
	// `midware:"auth" method:"post"`
	AddWithdrawal(ctx context.Context, req *DepositMs) (resp *EmptyReq, err error)

	//  添加充币地址
	// `midware:"auth" method:"post"`
	AddDeposit(ctx context.Context, req *DepositMs) (resp *EmptyReq, err error)

	//   法币资产
	// `midware:"auth" method:"get"`
	AccountBalance(ctx context.Context, req *AccountBalanceReq) (resp *AccountBalanceResp, err error)

	//    取消订单
	// `method:"post"`
	OrderCancel(ctx context.Context, req *OrderCancelReq) (resp *EmptyReq, err error)

	//    法币账单明细
	// `midware:"auth" method:"get"`
	DealHistory(ctx context.Context, req *DealHistoryReq) (resp *DealHistoryResp, err error)

	//    广告 开关接单
	// `midware:"auth" method:"get"`
	GetSwitchAds(ctx context.Context, req *EmptyReq) (resp *SwitchAdsReq, err error)

	//    广告 开关接单
	// `midware:"auth" method:"get"`
	SwitchAds(ctx context.Context, req *SwitchAdsReq) (resp *EmptyReq, err error)
}

var UserCenterSvc UserCenterBMServer

func userCenterPing(c *bm.Context) {
	p := new(google_protobuf1.Empty)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Ping(c, p)
	c.JSON(resp, err)
}

func userCenterCountryList(c *bm.Context) {
	p := new(EmptyReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.CountryList(c, p)
	c.JSON(resp, err)
}

func userCenterRegister(c *bm.Context) {
	p := new(RegisterReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Register(c, p)
	c.JSON(resp, err)
}

func userCenterLogin(c *bm.Context) {
	p := new(LoginReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Login(c, p)
	c.JSON(resp, err)
}

func userCenterAuthPing(c *bm.Context) {
	p := new(EmptyReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AuthPing(c, p)
	c.JSON(resp, err)
}

func userCenterAdvertiseList(c *bm.Context) {
	p := new(AdvertiseListReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AdvertiseList(c, p)
	c.JSON(resp, err)
}

func userCenterSync(c *bm.Context) {
	p := new(EmptyReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Sync(c, p)
	c.JSON(resp, err)
}

func userCenterDelAdvertise(c *bm.Context) {
	p := new(DelAdvertiseReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.DelAdvertise(c, p)
	c.JSON(resp, err)
}

func userCenterAddAdvertise(c *bm.Context) {
	p := new(AddAdvertiseReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AddAdvertise(c, p)
	c.JSON(resp, err)
}

func userCenterAuthAdvertise(c *bm.Context) {
	p := new(EmptyReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AuthAdvertise(c, p)
	c.JSON(resp, err)
}

func userCenterRealIdentity(c *bm.Context) {
	p := new(RealIdentityReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.RealIdentity(c, p)
	c.JSON(resp, err)
}

func userCenterAddPayWay(c *bm.Context) {
	p := new(AddPayWayReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AddPayWay(c, p)
	c.JSON(resp, err)
}

func userCenterOrderAdvertise(c *bm.Context) {
	p := new(OrderAdvertiseReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.OrderAdvertise(c, p)
	c.JSON(resp, err)
}

func userCenterOrderAdvertiseQuick(c *bm.Context) {
	p := new(OrderAdvertiseReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.OrderAdvertiseQuick(c, p)
	c.JSON(resp, err)
}

func userCenterComplain(c *bm.Context) {
	p := new(ComplainReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Complain(c, p)
	c.JSON(resp, err)
}

func userCenterConfirm(c *bm.Context) {
	p := new(ConfirmReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Confirm(c, p)
	c.JSON(resp, err)
}

func userCenterComplete(c *bm.Context) {
	p := new(CompleteReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Complete(c, p)
	c.JSON(resp, err)
}

func userCenterInternalComplete(c *bm.Context) {
	p := new(CompleteReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.InternalComplete(c, p)
	c.JSON(resp, err)
}

func userCenterMyCenter(c *bm.Context) {
	p := new(EmptyReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.MyCenter(c, p)
	c.JSON(resp, err)
}

func userCenterCurrencyList(c *bm.Context) {
	p := new(CurrencyListReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.CurrencyList(c, p)
	c.JSON(resp, err)
}

func userCenterAdvertiseInfo(c *bm.Context) {
	p := new(AdvertiseInfoReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AdvertiseInfo(c, p)
	c.JSON(resp, err)
}

func userCenterOrderInfo(c *bm.Context) {
	p := new(OrderDetailReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.OrderInfo(c, p)
	c.JSON(resp, err)
}

func userCenterUpdateMyCenter(c *bm.Context) {
	p := new(UpdateMyCenterReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.UpdateMyCenter(c, p)
	c.JSON(resp, err)
}

func userCenterAuthTread(c *bm.Context) {
	p := new(AuthTreadReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AuthTread(c, p)
	c.JSON(resp, err)
}

func userCenterOrderList(c *bm.Context) {
	p := new(AuthTreadReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.OrderList(c, p)
	c.JSON(resp, err)
}

func userCenterGetWithdrawal(c *bm.Context) {
	p := new(GetWithdrawalReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.GetWithdrawal(c, p)
	c.JSON(resp, err)
}

func userCenterBillList(c *bm.Context) {
	p := new(BillListReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.BillList(c, p)
	c.JSON(resp, err)
}

func userCenterDeposit(c *bm.Context) {
	p := new(DepositReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Deposit(c, p)
	c.JSON(resp, err)
}

func userCenterWithdrawal(c *bm.Context) {
	p := new(WithdrawalReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.Withdrawal(c, p)
	c.JSON(resp, err)
}

func userCenterAddWithdrawal(c *bm.Context) {
	p := new(DepositMs)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AddWithdrawal(c, p)
	c.JSON(resp, err)
}

func userCenterAddDeposit(c *bm.Context) {
	p := new(DepositMs)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AddDeposit(c, p)
	c.JSON(resp, err)
}

func userCenterAccountBalance(c *bm.Context) {
	p := new(AccountBalanceReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.AccountBalance(c, p)
	c.JSON(resp, err)
}

func userCenterOrderCancel(c *bm.Context) {
	p := new(OrderCancelReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.OrderCancel(c, p)
	c.JSON(resp, err)
}

func userCenterDealHistory(c *bm.Context) {
	p := new(DealHistoryReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.DealHistory(c, p)
	c.JSON(resp, err)
}

func userCenterGetSwitchAds(c *bm.Context) {
	p := new(EmptyReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.GetSwitchAds(c, p)
	c.JSON(resp, err)
}

func userCenterSwitchAds(c *bm.Context) {
	p := new(SwitchAdsReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := UserCenterSvc.SwitchAds(c, p)
	c.JSON(resp, err)
}

// RegisterUserCenterBMServer Register the blademaster route
func RegisterUserCenterBMServer(e *bm.Engine, server UserCenterBMServer) {
	UserCenterSvc = server
	e.GET("/exchange/usercenter/ping", userCenterPing)
	e.GET("/exchange/usercenter/country_list", userCenterCountryList)
	e.POST("/exchange/usercenter/register", userCenterRegister)
	e.POST("/exchange/usercenter/login", userCenterLogin)
	auth := e.Group("", authUser())
	auth.GET("/exchange/usercenter/auth_ping", userCenterAuthPing)
	e.GET("/exchange/usercenter/advertise_list", userCenterAdvertiseList)
	e.GET("/exchange/usercenter/sync", userCenterSync)
	auth.POST("/exchange/usercenter/del_advertise", userCenterDelAdvertise)
	auth.POST("/exchange/usercenter/add_advertise", userCenterAddAdvertise)
	auth.GET("/exchange/usercenter/auth_advertise", userCenterAuthAdvertise)
	auth.POST("/exchange/usercenter/real_identity", userCenterRealIdentity)
	auth.POST("/exchange/usercenter/add_pay_way", userCenterAddPayWay)
	auth.POST("/exchange/usercenter/order_advertise", userCenterOrderAdvertise)
	auth.POST("/exchange/usercenter/order_advertise_quick", userCenterOrderAdvertiseQuick)
	auth.POST("/exchange/usercenter/complain", userCenterComplain)
	auth.POST("/exchange/usercenter/confirm", userCenterConfirm)
	auth.POST("/exchange/usercenter/complete", userCenterComplete)
	e.POST("/exchange/usercenter/internal_complete", userCenterInternalComplete)
	auth.GET("/exchange/usercenter/my_center", userCenterMyCenter)
	e.GET("/exchange/usercenter/currency_list", userCenterCurrencyList)
	e.GET("/exchange/usercenter/advertise_info", userCenterAdvertiseInfo)
	auth.GET("/exchange/usercenter/order_info", userCenterOrderInfo)
	auth.POST("/exchange/usercenter/update_my_center", userCenterUpdateMyCenter)
	auth.POST("/exchange/usercenter/auth_tread", userCenterAuthTread)
	auth.GET("/exchange/usercenter/order_list", userCenterOrderList)
	auth.GET("/exchange/usercenter/get_withdrawal", userCenterGetWithdrawal)
	auth.GET("/exchange/usercenter/bill_list", userCenterBillList)
	auth.POST("/exchange/usercenter/deposit", userCenterDeposit)
	auth.POST("/exchange/usercenter/withdrawal", userCenterWithdrawal)
	auth.POST("/exchange/usercenter/add_withdrawal", userCenterAddWithdrawal)
	auth.POST("/exchange/usercenter/add_deposit", userCenterAddDeposit)
	auth.GET("/exchange/usercenter/account_balance", userCenterAccountBalance)
	e.POST("/exchange/usercenter/order_cancel", userCenterOrderCancel)
	auth.GET("/exchange/usercenter/deal_history", userCenterDealHistory)
	auth.GET("/exchange/usercenter/get_switch_ads", userCenterGetSwitchAds)
	auth.GET("/exchange/usercenter/switch_ads", userCenterSwitchAds)
}

// WalletBMServer is the server API for Wallet service.
// 定义Wallet服务
type WalletBMServer interface {
	AuthUser(ctx *bm.Context) (err error)

	// 合并接口 （测试）
	// `method:"get"`
	Sync(ctx context.Context, req *EmptyReq) (resp *SyncResp, err error)

	//    提现人民币
	// `midware:"auth" method:"post"`
	WithdrawalRmb(ctx context.Context, req *WithdrawalRmbReq) (resp *EmptyReq, err error)

	//    转账
	// `midware:"auth" method:"post"`
	Transaction(ctx context.Context, req *TransactionReq) (resp *EmptyReq, err error)

	//    兑汇
	// `midware:"auth" method:"post"`
	Transfers(ctx context.Context, req *TransfersReq) (resp *EmptyReq, err error)

	//    兑汇账单明细
	// `midware:"auth" method:"get"`
	TransfersHistory(ctx context.Context, req *DealHistoryReq) (resp *DealHistoryResp, err error)

	//    银行卡列表
	// `midware:"auth" method:"get"`
	BankList(ctx context.Context, req *EmptyReq) (resp *BankListResp, err error)

	// 设置默认银行卡
	DefaultBank(ctx context.Context, req *BankReq) (resp *Banks, err error)

	// 删除银行卡
	DelBank(ctx context.Context, req *BankReq) (resp *EmptyReq, err error)

	//   法币资产
	// `midware:"auth" method:"get"`
	AccountBalance(ctx context.Context, req *AccountBalanceReq) (resp *AccountBalanceResp, err error)

	// 验证资金密码
	// `midware:"auth" method:"post"`
	AuthTread(ctx context.Context, req *AuthTreadReq) (resp *EmptyReq, err error)
}

var WalletSvc WalletBMServer

func walletSync(c *bm.Context) {
	p := new(EmptyReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.Sync(c, p)
	c.JSON(resp, err)
}

func walletWithdrawalRmb(c *bm.Context) {
	p := new(WithdrawalRmbReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.WithdrawalRmb(c, p)
	c.JSON(resp, err)
}

func walletTransaction(c *bm.Context) {
	p := new(TransactionReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.Transaction(c, p)
	c.JSON(resp, err)
}

func walletTransfers(c *bm.Context) {
	p := new(TransfersReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.Transfers(c, p)
	c.JSON(resp, err)
}

func walletTransfersHistory(c *bm.Context) {
	p := new(DealHistoryReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.TransfersHistory(c, p)
	c.JSON(resp, err)
}

func walletBankList(c *bm.Context) {
	p := new(EmptyReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.BankList(c, p)
	c.JSON(resp, err)
}

func walletDefaultBank(c *bm.Context) {
	p := new(BankReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.DefaultBank(c, p)
	c.JSON(resp, err)
}

func walletDelBank(c *bm.Context) {
	p := new(BankReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.DelBank(c, p)
	c.JSON(resp, err)
}

func walletAccountBalance(c *bm.Context) {
	p := new(AccountBalanceReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.AccountBalance(c, p)
	c.JSON(resp, err)
}

func walletAuthTread(c *bm.Context) {
	p := new(AuthTreadReq)
	if err := c.BindWith(p, binding.Default(c.Request.Method, c.Request.Header.Get("Content-Type"))); err != nil {
		return
	}
	c.Context = context.WithValue(c.Context, "remote_addr", c.Request.RemoteAddr)
	c.Context = context.WithValue(c.Context, "userID", c.Request.Header.Get("userID"))
	c.Context = context.WithValue(c.Context, "token", c.Request.Header.Get("token"))
	resp, err := WalletSvc.AuthTread(c, p)
	c.JSON(resp, err)
}

// RegisterWalletBMServer Register the blademaster route
func RegisterWalletBMServer(e *bm.Engine, server WalletBMServer) {
	WalletSvc = server
	e.GET("/exchange/wallet/sync", walletSync)
	auth := e.Group("", authUser())
	auth.POST("/exchange/wallet/withdrawal_rmb", walletWithdrawalRmb)
	auth.POST("/exchange/wallet/transaction", walletTransaction)
	auth.POST("/exchange/wallet/transfers", walletTransfers)
	auth.GET("/exchange/wallet/transfers_history", walletTransfersHistory)
	auth.GET("/exchange/wallet/bank_list", walletBankList)
	e.GET("/exchange/wallet/default_bank", walletDefaultBank)
	e.GET("/exchange/wallet/del_bank", walletDelBank)
	auth.GET("/exchange/wallet/account_balance", walletAccountBalance)
	auth.POST("/exchange/wallet/auth_tread", walletAuthTread)
}

// 鉴权验证
func authUser() bm.HandlerFunc {
	return func(ctx *bm.Context) {
		err := UserCenterSvc.AuthUser(ctx)
		if err != nil {
			ctx.RetAuthorizeError(err.Error())
			ctx.Abort()
		}
	}
}
