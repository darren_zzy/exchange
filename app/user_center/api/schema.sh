#/bin/sh
filename=$1
packagename=`sed -n '/^option/'p $filename |awk '{print $4}'|sed -e 's/;//g' |sed -e 's/"//g'`

objs=( $(sed -n '/^message/'p $filename |awk '{print $2}'|xargs -I {} echo "*$packagename.{}") )

echo>tmp.json
echo "[" > tmp.json
for obj in "${objs[@]}"
do
	mid=`python -c "import binascii;print \"%s\" % (binascii.crc32(\"$obj\") & 0xFFFFFFFF)"`
	echo "{\"id\":$mid, \"name\":\"$obj\"}," >> tmp.json
done
sed -i "" '$s/},/}]/' tmp.json
jq . tmp.json > $filename.json
rm tmp.json