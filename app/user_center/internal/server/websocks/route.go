package websocks

import (
	"git.digittraders.com/exchange/api/websocket"
)

func register() {
	pb := MessagerInstance.(*Protobuf)
	pb.RegisterMethod(&websocket.OrderDetail{}, OrderDetail)
	pb.RegisterMethod(&websocket.OrderListReq{}, OrderList)
	pb.RegisterMethod(&websocket.OrderList{}, nil)

	// pb.RegisterMethod(&websocket.PayWay{}, test)
	// pb.RegisterMethod(&websocket.PayWay{}, test)
}
