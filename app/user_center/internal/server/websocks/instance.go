package websocks

import (
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/api/websocket"
	"git.digittraders.com/exchange/internal/model"
	"github.com/go-kratos/kratos/pkg/log"
)

func NewConnection(args []interface{}) {
	_, client, err := CheckArgs(args)
	if err != nil {
		log.Error(err.Error())
		return
	}
	log.Info("start NewConnection userId: %d", client.UserID)
}

// Heartbeat 处理客户端心跳报文
func Heartbeat(args []interface{}) {
	msg, client, err := CheckArgs(args)
	if err != nil {
		log.Error(err.Error())
		return
	}
	// if !client.isInStage(stage_authed) {
	// 	return
	// }
	hb, ok := msg.(*websocket.Heartbeat)
	if !ok {
		// client.Close()
		return
	}
	if hb.GetT() == 1 {
		client.SendHeartbeat(2)
	}
}

func Auth(args []interface{}) {
	msg, client, err := CheckArgs(args)
	if err != nil {
		log.Error(err.Error())
		return
	}

	var cmd int32
	var authMsg string
	if client.isInStage(StageAuthed) {
		cmd = websocket.CMD_value["AuthSuccess"]
		authMsg = "auth success"
	}
	if err = client.setStage(StageAuthing); err != nil {
		client.Close()
		return
	}

	body, ok := msg.(*websocket.Login)
	if !ok {
		cmd = websocket.CMD_value["AuthFalied"]
		authMsg = "auth falied"

		// result := &websocket.SystemCMD{Code: 1, Msg: "鉴权失败"}
		// if bs, err := MessagerInstance.Marshal(result); err == nil {
		// 	client.Send(bs)
		// }
		// client.Close()
		// return
	} else {
		if body.GetUserId() == 0 || body.GetToken() == "" {
			log.Warn("client[%d] auth faleid: payload error", body.GetUserId())
			client.Close()
			return
		}
		// ts, _ := strconv.ParseInt(l.GetP(), 10, 64)
		// if int64(math.Abs(float64(time.Now().Unix()-ts))) > 3600 {
		// 	icelog.Warnf("client[%d] auth falied: P[%s] error", l.GetUserId(), l.GetP())
		// 	client.Close()
		// 	return
		// }
		var user *model.Member
		if user, err = Dao.GetMember(&api.MemberReq{UserId: body.UserId}); err != nil {
			return
		}

		if user.Token == body.GetToken() {
			log.Info("成功鉴权！userId:%d", body.GetUserId())

			// success
			oldClient := WebServerInstance.GetClient(body.GetUserId())
			if oldClient != nil {
				log.Info("获取到旧客户端，删除旧客户端！userId:%d", body.GetUserId())
				WebServerInstance.RemoveClient(oldClient)
				oldClient.kickoff()
			}

			if err = client.setStage(StageAuthed); err != nil {
				client.Close()
				return
			}

			client.UserID = body.GetUserId()
			WebServerInstance.SetClient(client)
			cmd = websocket.CMD_value["AuthSuccess"]
			authMsg = "auth success"
			go client.KeepAlive()
			if mh := MessagerInstance.(*Protobuf).GetMethod(&websocket.NewConnection{}); mh != nil {
				go mh([]interface{}{nil, client})
			}
		} else {
			cmd = websocket.CMD_value["AuthFalied"]
			authMsg = "auth failed 123"
			log.Error("鉴权失败 %s %s", user.Token, body.GetToken())
		}
	}
	if bs, err := MessagerInstance.Marshal(&websocket.SystemCMD{
		Code: cmd,
		Msg:  authMsg,
	}); err == nil {
		client.Send(bs)
	}
}

func ConnectionClose(args []interface{}) {
	_, client, err := CheckArgs(args)
	if err != nil {
		log.Error(err.Error())
		return
	}
	// roomId, err := dao.GlobalBaseDao.CacheStore.GetUserRoomId(client.UserID)
	// if err != nil && roomId == 0 {
	// 	return
	// }

	// temp, ok := server.RoomMag.Rooms.Get(roomId)
	// if !ok {
	// 	return
	// }
	// room := temp.(*gb.Room)
	// if room.Event.Len() > 0 {
	// 	// 仅有初始等待时候才生效
	// 	return
	// }

	// var color int32
	// if room.White == client.UserID {
	// 	color = Black
	// } else if room.Black == client.UserID {
	// 	color = White
	// } else {
	// 	return
	// }

	// 广播游戏结果，跳回原生
	// m, _ := server.MessagerInstance.Marshal(&gs_gobang.Over{
	// 	R: roomId,
	// 	C: color, //输者
	// 	A: 2,     //1：输了 0 平局
	// })
	// server.Broadcast(room.Users, room.WsId, m)

	log.Info("ConnectionClose %d", client.UserID)
}
