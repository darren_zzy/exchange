function bytesToint(value){

  var a=new Uint32Array(1)

  a[0]= (value[0] & 0xFF)<<24

  a[0]=a[0] | ((value[1] & 0xFF) <<16)

  a[0]=a[0] | ((value[2] & 0xFF) <<8)

  a[0]=a[0] | (value[3] & 0xFF)

  return a
}

function intTobytes(value){
  var a=new Uint8Array(4)
  a[0]=(value >> 24) & 0xFF

  a[1]=(value >> 16) & 0xFF

  a[2]=(value >> 8) & 0xFF

  a[3]=value  & 0xFF

  return a;
}

protobuf.Root.prototype.resolvePath = function(o, t) { return t}

class SocketMessage {
  static cmds = null
  static cmdIds = null
  static protoRoot = null

  static async loadDepends (cmdJSONUrl, protoUrl) {
    const loadCmd = this.loadComands(cmdJSONUrl)
    const loadProto = this.loadProtobuf(protoUrl)
    await Promise.all([loadCmd, loadProto])
  }

  static async loadComands (cmdJSONUrl) {
    const {data: cmdIdName} = await axios.get(cmdJSONUrl)
    const cmds = []
    const cmdIds = []
    cmdIdName.forEach(j => {
      const name = j.name.substring(1)
      cmds.push(name)
      cmdIds.push(j.id)
    })
    SocketMessage.cmdIds = cmdIds
    SocketMessage.cmds = cmds
  }

  static async loadProtobuf (protoUrl) {
    const root = await protobuf.load(protoUrl)
    SocketMessage.protoRoot = root
  }

  constructor (nsp = 'websocket') {
    if (!SocketMessage.protoRoot || !SocketMessage.cmdIds) {
      throw new Error('必须先使用静态方法loadDepends初始化protobuf和注入commands')
    }
    this.nsp = nsp
    this.events = {}
    this.socket = null
    this.loopHeartbeat = null
  }

  init (socket) {
    socket.binaryType = 'arraybuffer'
    socket.onmessage = this.onmessage
    this.socket = socket
  }

  onmessage = ev => {
    const byteLength = ev.data.byteLength
    if (byteLength < 4) {
      console.error('data too short')
    } else {
      const id = bytesToint(new Uint8Array(ev.data.slice(0, 4)))[0]
      const typeIndex = SocketMessage.cmdIds.indexOf(id)
      if (typeIndex > -1) {
        const type = SocketMessage.cmds[typeIndex]
        const msgRef = SocketMessage.protoRoot.lookup(type)
        const msg = msgRef.decode(new Uint8Array(ev.data.slice(4)))
        console.error(type)
        if (this.events[type]) {
          this.events[type].forEach(cb => {
            cb(msg)
          })
        }
      }
    }
  }

  on (type, cb) {
    if (this.nsp) {
      type = `${this.nsp}.${type}`
    }
    if (SocketMessage.cmds.indexOf(type) > -1) {
      const events = this.events
      if (!events[type]) {
        events[type] = []
      }
      events[type].push(cb)
    }
  }

  off (type, cb) {
    if (this.nsp) {
      type = `${this.nsp}.${type}`
    }
    if (SocketMessage.cmds.indexOf(type) > -1) {
      const events = this.events
      if (events[type] && !cb) {
        events[type] = []
      } else if (events[type] && cb) {
        const index = events[type].indexOf(cb)
        if (index > -1) {
          events[type].splice(index, 1)
        }
      }
    }
  }

  emit (type, data) {
    // 向服务端发消息
    if (this.nsp) {
      type = `${this.nsp}.${type}`
    }
    const typeIndex = SocketMessage.cmds.indexOf(type)
    if (typeIndex > -1) {
      const msgId = intTobytes(SocketMessage.cmdIds[typeIndex])
      const tool = SocketMessage.protoRoot.lookup(type)
      const msgBuf = tool.encode(data).finish()
      const sendBuf = new Uint8Array(msgBuf.byteLength + 4)
      sendBuf.set(msgId, 0)
      sendBuf.set(msgBuf, 4)
      this.socket.send(sendBuf)
    }
  }

  close () {
    this.events = {}
    if (this.socket) {
      this.socket.close()
    }
    if (this.loopHeartbeat) {
      clearInterval(this.loopHeartbeat)
      this.loopHeartbeat = null
    }
  }

  heartbeat (time, action= 'Heartbeat') {
    this.emit(action, {t: 1})
    this.loopHeartbeat = setInterval(() => {
      this.emit(action, {t: 1})
    }, time * 1000)

    this.on(action, data => {
      if (data.t === 1) {
        this.emit(action, 2)
      }
    })
  }
}

