package websocks

import "C"
import (
	"flag"
	"git.digittraders.com/exchange/internal/dao"
	"git.digittraders.com/exchange/pkg/lib"
	"github.com/go-kratos/kratos/pkg/log"
	"github.com/gorilla/websocket"
	"net/http"
	"sync"
	"time"
)

var MessagerInstance Messager
var Dao dao.Dao
var WebServerInstance *Client

// 开一个页面用于测试ws
var addr = flag.String("addr", ":8080", "http service address")

// New new a dao and return.
func NewWs(d dao.Dao) (hub *Client, cf func(), err error) {
	flag.Parse()
	// 初始化配置
	cfg.NewConfig()

	hub = newHub()
	go hub.run()
	WebServerInstance = hub

	cf = func() { println("close ws") }
	MessagerInstance = NewProtobuf()
	// 行为注册
	register()

	// 注入dao层
	Dao = d
	// 启动ws监听
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		err := serveWs(hub, w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	})

	// 导入静态文件
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("../internal/server/websocks/static"))))

	http.Handle("/websocket/", http.StripPrefix("/websocket/", http.FileServer(http.Dir("../api/websocket"))))

	http.HandleFunc("/", serveHome)

	go http.ListenAndServe(*addr, nil)

	return
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "../internal/server/websocks/marshal.html")
}

func newHub() *Client {
	return &Client{
		Broadcast:  make(chan []byte),
		register:   make(chan *User),
		unregister: make(chan *User),
		clients:    make(map[*User]ClientInfo),
		Users:      make(map[int64]*User),
	}
}

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	// hub *Hub
	Users map[int64]*User
	// Inbound messages from the clients.
	Broadcast chan []byte

	// Registered clients.
	clients map[*User]ClientInfo
	// Register requests from the clients.
	register chan *User

	// Unregister requests from clients.
	unregister chan *User
	sync.RWMutex

	// The websocket connection.
	// conn *websocket.Conn

	// Buffered channel of outbound messages.
	// send chan []byte
}

func (h *Client) run() {
	for {
		select {
		case client := <-h.register:

			h.clients[client] = ClientInfo{
				exist: true,
			}

		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				log.Info("关闭 channal！！！ 客户端")
				delete(h.clients, client)
				// close(client.Close())
			}
		case message := <-h.Broadcast:
			for userId, client := range h.Users {
				select {
				case client.messageChan <- message:

				default:
					log.Info("关闭 channal！！！ 客户端", userId)
					close(client.messageChan)
					delete(h.Users, userId)
				}
			}
			// case msg := <-h.single:

			// msg

			// if user, ok := h.Users[msg.]; ok {

			// select {
			// 	case client.send <- msg.message:
			// 		client.send <- msg.message
			// 	default:
			// 		close(client.send)
			// 		delete(h.clients, client.Client)
			// 	}
			// }

		}
	}
}

// serveWs handles websocket requests from the peer.
func serveWs(hub *Client, w http.ResponseWriter, r *http.Request) (err error) {
	// userIds, ok := r.URL.Query()["user_id"]
	// if !ok {
	// 	return
	// }
	// // fmt.Println(r.RequestURI, 3333, userIds)
	// userId := userIds[0]
	// if userId == "" {
	// 	// _, errs := upgrader.Errors(w, r, nil)
	// 	// log.Println(errs)
	// 	http.Error(w, "12", 200)
	// 	return
	// }
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Info(err.Error())
		return
	}

	remote := lib.GetRemoteIp(conn.RemoteAddr().String())
	user := &User{
		conn:           conn,
		messageChan:    make(chan []byte, 256),
		remoteAddr:     remote,
		exitChan:       make(chan struct{}),
		lastActiveTime: time.Now(),
		stage:          StageAuthing,
	}
	// 注入
	hub.register <- user

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go user.writePump()
	go user.readPump()
	return
}

func (h *Client) RemoveClient(c *User) {
	if c.UserID == 0 {
		return
	}
	h.Lock()
	defer h.Unlock()
	delete(h.Users, c.UserID)
	delete(h.clients, c)
}

func (gs *Client) GetClient(userID int64) *User {
	if userID == 0 {
		return nil
	}
	gs.Lock()
	defer gs.Unlock()
	return gs.Users[userID]
}

func (gs *Client) SetClient(c *User) {
	if c.UserID == 0 {
		return
	}
	gs.Lock()
	log.Info("设置新客户端！userId:%d", c.UserID)
	gs.Users[c.UserID] = c
	gs.Unlock()
}
