package websocks

type Messager interface {
	// proto.message
	Marshal(interface{}) ([]byte, error)
	// message
	Unmarshal([]byte) (interface{}, error)
	// message_body, client
	Route(interface{}, *User) error
}
