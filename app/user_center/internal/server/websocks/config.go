package websocks

import (
	"github.com/go-kratos/kratos/pkg/conf/paladin"
	"github.com/go-kratos/kratos/pkg/log"
	"time"
)

var cfg *Config

type Config struct {
	Network                  string
	Addr                     string
	Timeout                  int64
	ClientTimeoutMinutes     int64
	HeartbeatIntervalSeconds int64

	// 客户端超时分
	ClientTimeout time.Duration

	// 心跳超时秒
	HeartbeatInterval time.Duration
}

func (c *Config) NewConfig() {
	var (
		ct  paladin.TOML
		err error
	)
	if err = paladin.Get("ws.toml").Unmarshal(&ct); err != nil {
		return
	}
	if err = ct.Get("Server").UnmarshalTOML(&cfg); err != nil {
		log.Error(err.Error(), "（服务初始化失败，参数错误）")
		return
	}

	if cfg.ClientTimeoutMinutes == 0 {
		cfg.ClientTimeout = 3 * time.Minute

	} else {
		cfg.ClientTimeout = time.Duration(cfg.ClientTimeoutMinutes) * time.Minute
	}

	if cfg.HeartbeatIntervalSeconds == 0 {
		cfg.HeartbeatInterval = 15 * time.Second

	} else {
		cfg.HeartbeatInterval = time.Duration(cfg.HeartbeatIntervalSeconds) * time.Second
	}
}
