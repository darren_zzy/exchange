package websocks

import (
	"errors"
	wspb "git.digittraders.com/exchange/api/websocket"
	"github.com/go-kratos/kratos/pkg/log"
	"github.com/gorilla/websocket"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

type Stage int

var (
	StageAuthing = Stage(1)
	StageAuthed  = Stage(2)
	StageKicked  = Stage(3)
	StageClosing = Stage(4)
	StageClosed  = Stage(5)
)

var StagDesc = map[Stage]string{
	StageAuthing: "AUTHING",
	StageAuthed:  "AUTHED",
	StageKicked:  "KICKED",
	StageClosing: "CLOSING",
	StageClosed:  "CLOSED",
}

type ClientInfo struct {
	exist  bool
	userId int64
}

type User struct {
	sync.RWMutex
	lastActiveTime time.Time
	remoteAddr     string
	UserID         int64
	conn           *websocket.Conn
	// 报文序列号
	packetSeq uint32

	// 发消息
	messageChan chan []byte
	// 关闭信号
	exitChan chan struct{}
	// 当前状态
	stage Stage
}

// GetSeq 获取客户端报文序号
func (c *User) getSeq() uint32 {
	return atomic.AddUint32(&c.packetSeq, 1)
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *User) readPump() {
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	var mi interface{}
	for {
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Info("error: %v", err)
			}
			log.Error("消息读取失败 1, err: %s", err.Error())
			go c.Close()
			break
		}
		mi, err = MessagerInstance.Unmarshal(message)
		if err != nil {
			log.Error("消息读取失败 2, err: %s", err.Error())
			break
		}

		err = MessagerInstance.Route(mi, c)
		if err != nil {
			log.Error("消息读取失败 3, err %s:", err.Error())
			break
		}
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *User) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		// c.Close()
	}()
	for {
		select {
		case message, ok := <-c.messageChan:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				continue
			}

			w, err := c.conn.NextWriter(websocket.BinaryMessage)
			if err != nil {
				continue
			}
			w.Write(message)
			if err := w.Close(); err != nil {
				continue
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		case <-ticker.C:

		}
	}
}

func (c *User) UpdateLastActiveTime() {
	c.lastActiveTime = time.Now()
}

// KeepAlive 定时心跳检查客户端状态，踢出超时客户端
func (c *User) KeepAlive() {
	log.Info("开始心跳！！: %d", c.UserID)

	var err error
	ticker := time.NewTicker(10 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-c.exitChan:
			goto exit
		case <-ticker.C:
			if time.Since(c.lastActiveTime) > cfg.ClientTimeout {
				// client timeout
				log.Info("客户端超时3m，上次更新时间：%d  用户 id： %d", c.lastActiveTime.Unix(), c.UserID)
				go c.Close()
			} else if time.Since(c.lastActiveTime) > cfg.HeartbeatInterval {
				log.Info("距离上次心跳15s以上%d,  用户 id：%d", c.lastActiveTime.Unix(), c.UserID)
				go c.SendHeartbeat(1)
			}
		}
	}
exit:
	log.Info("client %d exiting KeepAlive, err=%v", c.UserID, err)
}

// Send 发消息到客户端
func (c *User) Send(msg []byte) error {
	select {
	case c.messageChan <- msg:
	case <-time.After(time.Second):
		return errors.New("message send timeout")
	}
	return nil
}

// asyncAuthTimeout 认证超时
func (c *User) asyncAuthTimeout() {
	if bs, err := MessagerInstance.Marshal(&wspb.SystemCMD{
		Code: 0,
		Msg:  "auth timeout",
	}); err == nil {
		c.Send(bs)
	}
	// c.Close()
}

// kickoff 异地登陆被踢出
func (c *User) kickoff() {
	log.Info("异地登陆被踢出：%d  用户 id： %d", c.lastActiveTime.Unix(), c.UserID)
	if err := c.setStage(StageKicked); err != nil {
		log.Info("踢出失败：66666666")

		return
	}
	if bs, err := MessagerInstance.Marshal(&wspb.SystemCMD{
		Code: wspb.CMD_value["Kickoff"],
		Msg:  "login on another device",
	}); err == nil {
		c.Send(bs)
	}
	c.Close()
}

// isInStage 判断客户端当前状态
func (c *User) isInStage(stage Stage) bool {
	c.RLock()
	defer c.RUnlock()
	return c.stage == stage
}

// setStage 修改客户端状态
func (c *User) setStage(stage Stage) error {
	c.Lock()
	defer c.Unlock()
	if c.stage != StageAuthed && stage < c.stage {
		log.Error("[%d] setStage from [%s] to [%s] %s", c.UserID, StagDesc[c.stage], StagDesc[stage], c.conn.RemoteAddr())
		return errors.New("new stage is < old stage")
	}
	c.stage = stage
	return nil
}

// prepareClosing 准备关闭客户端
func (c *User) prepareClosing() (Stage, error) {
	c.Lock()
	defer c.Unlock()
	stage_saved := c.stage
	if c.stage >= StageClosing {
		return c.stage, errors.New("client is Closing or Closed")
	}
	c.stage = StageClosing
	return stage_saved, nil
}

// Close 关闭客户端
func (c *User) Close() {
	oldStage, err := c.prepareClosing()
	if err != nil {
		return
	}
	if c.UserID > 0 && oldStage != StageKicked {
		// 移除客户端
		WebServerInstance.RemoveClient(c)
	}
	log.Info("Ws Client[%d] close, old stage=%v %s", c.UserID, oldStage, c.remoteAddr)
	c.conn.Close()
	close(c.exitChan)
	close(c.messageChan)
	c.setStage(StageClosed)

	if mh := MessagerInstance.(*Protobuf).GetMethod(&wspb.ConnectionClose{}); mh != nil {
		go mh([]interface{}{nil, c})
	}
}

// SendHeartbeat 给客户端发送心跳报文
// typ 心跳类型 1:请求 2:响应
func (c *User) SendHeartbeat(typ uint32) error {
	heartbeat := &wspb.Heartbeat{
		Seq: c.getSeq(),
		P:   strconv.FormatInt(time.Now().Unix(), 10),
		T:   typ,
	}
	bs, err := MessagerInstance.Marshal(heartbeat)
	if err != nil {
		return err
	}
	return c.Send(bs)
}
