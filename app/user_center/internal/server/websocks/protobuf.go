package websocks

import (
	"encoding/binary"
	"errors"
	"fmt"
	"git.digittraders.com/exchange/api/websocket"
	"github.com/go-kratos/kratos/pkg/log"
	"github.com/golang/protobuf/proto"
	"hash/crc32"
	"reflect"
)

type Protobuf struct {
	methods     map[uint32]methodHandler
	messages    map[reflect.Type]MsgInfo
	message_ids map[uint32]reflect.Type
}

type MethodInfo struct {
	ID      uint32      `json:id`
	Message interface{} `json:message`
}

type MsgInfo struct {
	ID  uint32
	Typ reflect.Type
}

type methodHandler func([]interface{})

func NewProtobuf() *Protobuf {
	pb := &Protobuf{
		methods:     make(map[uint32]methodHandler),
		messages:    make(map[reflect.Type]MsgInfo),
		message_ids: make(map[uint32]reflect.Type),
	}
	pb.register_internal_methods()
	return pb
}

// register_internal_methods 注册系统内置函数
func (pb *Protobuf) register_internal_methods() {

	pb.RegisterMethod(&websocket.Heartbeat{}, Heartbeat)
	pb.RegisterMethod(&websocket.Login{}, Auth)
	pb.RegisterMethod(&websocket.SystemCMD{}, nil)
	pb.RegisterMethod(&websocket.ContentMessage{}, nil)
	pb.RegisterMethod(&websocket.NewConnection{}, NewConnection)
	pb.RegisterMethod(&websocket.ConnectionClose{}, ConnectionClose)
}

func (pb *Protobuf) RegisterMethod(pbMsg proto.Message, m methodHandler) {
	t := reflect.TypeOf(pbMsg)
	msgID := crc32.ChecksumIEEE([]byte(t.String()))
	if _, ok := pb.methods[msgID]; ok {
		panic(fmt.Sprintf("function id %v: already registered", msgID))
	}
	pb.messages[t] = MsgInfo{
		ID:  msgID,
		Typ: t,
	}
	pb.message_ids[msgID] = t
	if m != nil {
		pb.methods[msgID] = m
	}
}

func (pb *Protobuf) GetMethod(pbMsg proto.Message) methodHandler {
	t := reflect.TypeOf(pbMsg)
	msgID := crc32.ChecksumIEEE([]byte(t.String()))
	if mh, ok := pb.methods[msgID]; ok {
		return mh
	} else {
		log.Error("\n 消息 id  %+v  未注册在 pb.methods map中，请查看：%v", msgID, t)
	}
	return nil
}

func (pb *Protobuf) Marshal(msg interface{}) ([]byte, error) {
	t := reflect.TypeOf(msg)
	msgInfo, ok := pb.messages[t]
	if !ok {
		log.Error("\n not registered 消息体： %+v", msgInfo)
		return nil, errors.New("not registered")
	}
	m, ok := msg.(proto.Message)
	if !ok {
		log.Error("\n not protobuf 消息体： %+v  proto：%+v", msgInfo, m)
		return nil, errors.New("not protobuf")
	}
	bs, err := proto.Marshal(m)
	if err != nil {
		log.Error("\n proto.Marshal fail, 消息体： %+v  proto：%s", msgInfo, err.Error())
		return nil, err
	}
	result := make([]byte, len(bs)+4)
	binary.BigEndian.PutUint32(result, msgInfo.ID)
	log.Info("\n 发消息 ：%+v  %+v", msgInfo, msg)
	copy(result[4:], bs)
	return result, nil
}

func (pb *Protobuf) Unmarshal(msg []byte) (interface{}, error) {
	if len(msg) < 4 {
		return nil, errors.New("payload is too short")
	}
	id := binary.BigEndian.Uint32(msg[:4])
	typ, ok := pb.message_ids[id]
	if !ok {
		return nil, fmt.Errorf("message_id %d not found %+v", id, msg[:4])
	}
	dest, ok := reflect.New(typ.Elem()).Interface().(proto.Message)
	if !ok {
		return nil, fmt.Errorf("invalid dest(%#+v)", dest)
	}
	if err := proto.Unmarshal(msg[4:], dest); err != nil {
		log.Error("Unmarshal desc(%#+v) error %s", dest, err.Error())
		return nil, err
	}
	log.Info("收到消息-> result:(%#+v) byte:  %v ", dest, msg)

	return MethodInfo{
		ID:      id,
		Message: dest,
	}, nil
}

// 寻找路由
func (pb *Protobuf) Route(msgInfo interface{}, client *User) error {
	// log.Info("路由后，更新活跃时间")
	client.UpdateLastActiveTime()
	mi, ok := msgInfo.(MethodInfo)
	if !ok {
		return fmt.Errorf("Route error %#+v", msgInfo)
	}
	method, ok := pb.methods[mi.ID]
	// fmt.Printf("Route:  %+v,,,,,%+v,,  %+v \n", pb.methods, mi.Message, msgInfo)
	if !ok {
		return fmt.Errorf("Method %d not registered", mi.ID)
	}
	method([]interface{}{mi.Message, client})
	return nil
}

// 解析消息体和资源句柄
func CheckArgs(args []interface{}) (msg interface{}, client *User, err error) {
	if len(args) < 2 {
		err = fmt.Errorf("args is too short")
		return
	}
	var ok bool
	msg = args[0]
	client, ok = args[1].(*User)
	if !ok {
		err = fmt.Errorf("invalid client object")
		return
	}
	return
}
