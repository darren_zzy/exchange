package websocks

import (
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/api/websocket"
	"git.digittraders.com/exchange/internal/model"
	"git.digittraders.com/exchange/paramers"
	"github.com/go-kratos/kratos/pkg/log"
	"strconv"
	"time"
)

func OrderDetail(args []interface{}) {
	msg, client, err := CheckArgs(args)
	if err != nil {
		log.Error(err.Error())
		return
	}
	eee, ok := msg.(*websocket.OrderDetail)
	if !ok {
		client.Close()
		return
	}

	order, err := Dao.OrderDetail(eee.OrderId)
	if err != nil {
		return
	}

	if client.UserID != order.TraderID && client.UserID != order.SponsorID {
		client.SendErr("不是你的订单，无权操作")
	}

	resp := new(websocket.OrderDetail)

	var pay_user_id int64
	if order.AdvertiseType == paramers.OTC_SELL_TYPE {
		pay_user_id = order.SponsorID
	} else {
		pay_user_id = order.TraderID
	}

	pay_list := make([]*websocket.PayInfoWs, 0)
	list, err := Dao.GetMemberProfiles(pay_user_id)
	if err == nil {
		for _, v := range list {
			pay_list = append(pay_list, &websocket.PayInfoWs{
				PayWay:      v.Type,
				PayNum:      v.BoundNum,
				PayUserName: v.RealName,
				PayImage:    v.FontImage,
			})
		}
	}

	timeout := 900 - (time.Now().Unix() - order.CreateAt)

	var user2 *model.Member
	if order.SponsorID == client.UserID {
		user2, err = Dao.GetMember(&api.MemberReq{UserId: order.TraderID})
	} else {
		user2, err = Dao.GetMember(&api.MemberReq{UserId: order.SponsorID})

	}
	if err == nil {
		resp = &websocket.OrderDetail{
			Status:        order.Status,
			OrderId:       order.OrderID,
			TraderId:      order.TraderID,
			SponsorId:     order.SponsorID,
			AdvertiseType: order.AdvertiseType,
			Amount:        strconv.FormatFloat(order.Amount, 'f', -1, 64),
			PayInfo:       pay_list,
			Timeout:       timeout,
			UserName:      user2.Username,
			Price:         strconv.FormatFloat(order.Price, 'f', -1, 64),
			Total:         strconv.FormatFloat(order.Total, 'f', -1, 64),
			AdvertiseId:   order.AdvertiseId,
		}
		mm, err := MessagerInstance.Marshal(resp)
		if err == nil {
			client.Send(mm)
		}
	}

}

func OrderList(args []interface{}) {
	msg, client, err := CheckArgs(args)
	if err != nil {
		log.Error(err.Error())
		return
	}
	req, ok := msg.(*websocket.OrderListReq)
	if !ok {
		log.Info("ok: %+v  eee: %+v msg: %+v \n\n\n", ok, req, msg)
		client.Close()
		return
	}
	if req.Status == 0 {
		client.SendErr("")
		return
	}

	if !client.IsAuth() {
		client.SendAuthErr()
		return
	}
	status := strconv.FormatInt(req.Status, 10)
	userId := strconv.FormatInt(client.UserID, 10)
	list, err := Dao.OrderList(status, userId)
	resp := new(websocket.OrderList)
	resp.Type = req.Status

	for _, order := range list {
		var user2 *model.Member
		if order.SponsorID == client.UserID {
			user2, err = Dao.GetMember(&api.MemberReq{UserId: order.TraderID})
		} else {
			user2, err = Dao.GetMember(&api.MemberReq{UserId: order.SponsorID})
		}
		if err != nil {
			continue
		}

		timeout := 900 - (time.Now().Unix() - order.CreateAt)

		// 自动取消超时订单
		if timeout < 0 {
			if order.Status == paramers.ORDER_START {
				// 未付款超时 取消
				err = Dao.Cancel(order.OrderID)

			} else {
				// 已付款超时 走申诉
				var complain_id int64
				if order.AdvertiseType == paramers.OTC_SELL_TYPE {
					complain_id = order.TraderID
				} else {
					complain_id = order.SponsorID
				}
				_, err = Dao.Complain(&api.ComplainReq{OrderId: order.OrderID, UserId: complain_id})

			}
			continue
		}

		resp.List = append(resp.List, &websocket.OrderDetail{
			Status:        order.Status,
			OrderId:       order.OrderID,
			TraderId:      order.TraderID,
			SponsorId:     order.SponsorID,
			AdvertiseType: order.AdvertiseType,
			Amount:        strconv.FormatFloat(order.Amount, 'f', -1, 64),
			Timeout:       timeout,
			UserName:      user2.Username,
			Price:         strconv.FormatFloat(order.Price, 'f', -1, 64),
			Total:         strconv.FormatFloat(order.Total, 'f', -1, 64),
			AdvertiseId:   order.AdvertiseId,
		})
	}

	println(userId, "订单列表 用户！！！！")
	mm, err := MessagerInstance.Marshal(resp)
	client.Send(mm)
}

func (c *User) IsAuth() bool {
	if c.UserID > 0 {
		return true
	}
	return false
}

func (client *User) SendErr(msg string) {
	if msg == "" {
		msg = "错误！！！"
	}
	resp := &websocket.SystemCMD{Msg: msg, Code: 5}
	mm, _ := MessagerInstance.Marshal(resp)
	client.Send(mm)
	return
}

func (client *User) SendAuthErr() {
	resp := &websocket.SystemCMD{Msg: "未鉴权", Code: 1}
	mm, _ := MessagerInstance.Marshal(resp)
	client.Send(mm)
	return
}
