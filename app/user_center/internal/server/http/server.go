package http

import (
	pb "git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/internal/service"
	"github.com/go-kratos/kratos/pkg/conf/paladin"
	"github.com/go-kratos/kratos/pkg/log"
	bm "github.com/go-kratos/kratos/pkg/net/http/blademaster"
	xtime "github.com/go-kratos/kratos/pkg/time"
)

var (
	svc *service.Service
	// authSrv *permit.Permit
)

type Config struct {
	Network                  string
	Addr                     string
	Timeout                  xtime.Duration
	ClientTimeoutMinutes     int64
	HeartbeatIntervalSeconds int64
}

// New new a bm server.
func New(s *service.Service) (engine *bm.Engine, err error) {
	var (
		cfg bm.ServerConfig
		ct  paladin.TOML
		// authCfg permit.Config
	)
	if err = paladin.Get("http.toml").Unmarshal(&ct); err != nil {
		return
	}
	if err = ct.Get("Server").UnmarshalTOML(&cfg); err != nil {
		log.Error(err.Error(), "（服务初始化失败，参数错误）")

		return
	}
	// // 导入鉴权配置
	// if err = ct.Get("Auth").UnmarshalTOML(&authCfg); err != nil {
	// 	log.Error(err.Error(), "（鉴权初始化失败，参数错误）")
	// 	return
	// }
	svc = s
	engine = bm.DefaultServer(&cfg)
	// authSrv,err = permit.New(&authCfg)
	// 注册pb的 请求方法
	pb.RegisterUserCenterBMServer(engine, s)
	pb.RegisterWalletBMServer(engine, s)

	err = engine.Start()
	return
}
