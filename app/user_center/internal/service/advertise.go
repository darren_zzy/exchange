package service

import (
	"context"
	"fmt"
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/pkg/lib"
	"github.com/go-kratos/kratos/pkg/ecode"
	"github.com/go-kratos/kratos/pkg/log"
	"strconv"
	"strings"
)

func (s *Service) AdvertiseList(c context.Context, req *api.AdvertiseListReq) (resp *api.AdvertiseListResp, err error) {
	if req.GetCoin() == "" && req.GetType() == 0 {
		err = ecode.Error(ecode.RequestErr, fmt.Sprintf("%s (coin:%s & type:%d || user_id:%d)", ecode.ErrParamMsg, req.GetCoin(), req.GetType(), req.GetUserId()))
		return
	}
	resp = &api.AdvertiseListResp{}
	resp.List = make([]*api.Advertise, 0)
	resp.TradeFee = "0"
	// 广告列表
	list, count, err := s.dao.OtcAdvertiseList(req)
	if err != nil {
		log.Info(err.Error())
		return
	} else if len(list) == 0 {
		err = ecode.Error(ecode.OK, "广告为空")
		log.Info(err.Error())
		return
	}

	// 合成响应列表
	for _, v := range list {
		user, err := s.dao.GetMember(&api.MemberReq{UserId: v.OwnerID})
		if err != nil || user.ID == 0 {
			count--
			continue
		}
		trade, err := s.dao.GetUserTradingInfo(v.OwnerID)
		if err != nil {
			count--
			continue
		}
		var timeout int64
		if v.Timeout == 0 {
			timeout = -1
		} else {
			timeout = v.Timeout
		}
		resp.List = append(resp.List, &api.Advertise{
			Id:           v.ID,
			Username:     user.Username,
			Avatar:       user.Avatar,
			TotalOrders:  trade.GetTotal(),
			Closing:      trade.GetClosing(),
			EnableAmount: strconv.FormatFloat(v.RemainAmount, 'f', -1, 64),
			MaxLimit:     strconv.FormatFloat(v.MaxLimit, 'f', -1, 64),
			MinLimit:     strconv.FormatFloat(v.MinLimit, 'f', -1, 64),
			Price:        strconv.FormatFloat(v.Price, 'f', -1, 64),
			CoinId:       v.CoinID,
			Type:         v.Type,
			Timeout:      timeout,
		})
	}

	resp.Counts = count
	return
}

func (s *Service) AuthAdvertise(c context.Context, req *api.EmptyReq) (resp *api.EmptyReq, err error) {
	if !s.dao.HasRealName(c) {
		err = ecode.Error(ecode.UnauthorizedUser, "未实名认证")
		return
	}
	if !s.dao.HasQuality(c) {
		err = ecode.Error(ecode.UnauthorizedTread, "至少添加一种支付方式")
		return
	}
	return
}

func (s *Service) AddAdvertise(c context.Context, req *api.AddAdvertiseReq) (resp *api.EmptyReq, err error) {

	if req.GetPrice() == "" || req.GetMaxLimit() == "" || req.GetTotalAmount() == "" || req.GetCoinId() == 0 || req.GetType() == 0 || req.GetPayWay() == "" {
		err = ecode.Error(ecode.RequestErr, fmt.Sprintf("%s ", ecode.ErrParamMsg))
		return
	}
	if req.GetMinLimit() == "" {
		req.MinLimit = "100"
	}
	err = s.dao.AddAdvertise(c, req)
	return
}

// 实名认证
func (s *Service) RealIdentity(c context.Context, req *api.RealIdentityReq) (resp *api.EmptyReq, err error) {
	if req.GetUserId() == 0 || req.GetType() == 0 || req.GetBackImage() == "" || req.GetFrontImage() == "" || req.GetHandImage() == "" || req.GetRealName() == "" {
		err = ecode.Error(ecode.RequestErr, fmt.Sprintf("%s ", ecode.ErrParamMsg))
		return
	}
	err = s.dao.RealIdentity(req)
	return
}

// 添加新的支付方式
func (s *Service) AddPayWay(c context.Context, req *api.AddPayWayReq) (resp *api.EmptyReq, err error) {
	if req.GetBoundNum() == "" || req.GetTypeId() == 0 {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	if req.TypeId == 3 {
		if req.GetBankCardAuth() == "" {
			err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
			return
		}
		if !lib.VerifyPhone(req.GetPhone()) {
			err = ecode.Error(ecode.RequestErr, "手机号不合法")
			return
		}
	}
	if req.UserId, err = strconv.ParseInt(c.Value("userID").(string), 10, 64); err != nil {
		return
	}
	err = s.dao.AddPayWay(req)
	return
}

func (s *Service) DelAdvertise(c context.Context, req *api.DelAdvertiseReq) (resp *api.EmptyReq, err error) {
	if req.GetId() == 0 {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	err = s.dao.DelAdvertise(req.Id)
	return
}

// 广告信息
func (s *Service) AdvertiseInfo(c context.Context, req *api.AdvertiseInfoReq) (resp *api.Advertise, err error) {

	var userId int64
	if userId, err = strconv.ParseInt(c.Value("userID").(string), 10, 64); err != nil {
		return
	}
	user, err := s.dao.GetMember(&api.MemberReq{UserId: userId})
	if err != nil {
		return
	}

	trade, err := s.dao.GetUserTradingInfo(userId)
	if err != nil {
		return
	}

	data, err := s.dao.GetAdvertise(req.GetId())
	if err != nil {
		return
	}
	resp = &api.Advertise{
		Id:           req.GetId(),
		Username:     user.Username,
		Avatar:       user.Avatar,
		TotalOrders:  trade.GetTotal(),
		Closing:      trade.GetClosing(),
		EnableAmount: strconv.FormatFloat(data.RemainAmount, 'f', -1, 64),
		MaxLimit:     strconv.FormatFloat(data.MaxLimit, 'f', -1, 64),
		MinLimit:     strconv.FormatFloat(data.MinLimit, 'f', -1, 64),
		Price:        strconv.FormatFloat(data.Price, 'f', -1, 64),
		Way:          strings.Split(data.PayMode, ","),
		CoinId:       data.CoinID,
		Type:         data.Type,
		Timeout:      data.Timeout,
	}
	return
}

//    开关接单
func (s *Service) SwitchAds(c context.Context, req *api.SwitchAdsReq) (resp *api.EmptyReq, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)

	if req.GetStatus() != 1 || req.GetStatus() != 2 {
		err = ecode.Error(ecode.ServerErr, ecode.ErrParamMsg)
	}
	err = s.dao.SwitchOrder(req.Status, userId)
	return
}

//    获取开关接单
func (s *Service) GetSwitchAds(c context.Context, req *api.EmptyReq) (resp *api.SwitchAdsReq, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)

	err = s.dao.GetSwitchOrder(userId)
	resp = new(api.SwitchAdsReq)
	resp.Status = 1
	return
}
