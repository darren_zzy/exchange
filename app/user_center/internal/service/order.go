package service

import (
	"context"
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/api/websocket"
	"git.digittraders.com/exchange/internal/server/websocks"
	"git.digittraders.com/exchange/paramers"
	"github.com/go-kratos/kratos/pkg/ecode"
	"strconv"
	"time"
)

// 广告下单
func (s *Service) OrderAdvertise(c context.Context, req *api.OrderAdvertiseReq) (resp *api.OrderAdvertiseResp, err error) {
	if req.GetAdvertiseId() == 0 || req.GetAmount() == "" {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	user_id, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	if err != nil {
		return
	}
	resp = new(api.OrderAdvertiseResp)

	var sponsor_id int64
	resp.OrderId, sponsor_id, err = s.dao.BookOtcOrder(req, user_id)

	if mh := websocks.MessagerInstance.(*websocks.Protobuf).GetMethod(&websocket.OrderListReq{}); mh != nil {

		if client, ok := websocks.WebServerInstance.Users[sponsor_id]; ok {
			println("准备给客户端发消息 发起来！！！", sponsor_id)

			go mh([]interface{}{&websocket.OrderListReq{Status: 1}, client})
		} else {
			println("没找到用户", sponsor_id)
		}
	}

	return
}

// 快捷广告下单
func (s *Service) OrderAdvertiseQuick(c context.Context, req *api.OrderAdvertiseReq) (resp *api.OrderAdvertiseResp, err error) {
	if req.GetCoinId() == 0 || req.GetAmount() == "" {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	user_id, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	if err != nil {
		return
	}

	resp = new(api.OrderAdvertiseResp)
	var sponsor_id int64

	resp.OrderId, sponsor_id, err = s.dao.BookOtcOrder(req, user_id)

	// 发消息
	if mh := websocks.MessagerInstance.(*websocks.Protobuf).GetMethod(&websocket.OrderListReq{}); mh != nil {

		if client, ok := websocks.WebServerInstance.Users[sponsor_id]; ok {
			println("准备给客户端发消息 发起来！！！", sponsor_id)

			go mh([]interface{}{&websocket.OrderListReq{Status: 1}, client})
		} else {
			println("没找到用户", sponsor_id)
		}
	}

	return
}

// 申诉订单
func (s *Service) Complain(c context.Context, req *api.ComplainReq) (resp *api.EmptyReq, err error) {
	if req.GetOrderId() == "" {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	req.UserId, err = strconv.ParseInt(c.Value("userID").(string), 10, 64)
	if err != nil {
		return
	}
	order, err := s.dao.Complain(req)
	if err != nil {
		return
	}
	// 发消息通知 取反用户
	var resId int64
	if order.SponsorID == req.UserId {
		resId = order.TraderID
	} else {
		resId = order.SponsorID
	}
	if mh := websocks.MessagerInstance.(*websocks.Protobuf).GetMethod(&websocket.OrderListReq{}); mh != nil {
		if client, ok := websocks.WebServerInstance.Users[resId]; ok {
			go mh([]interface{}{&websocket.OrderListReq{Status: 1}, client})
		}
	}
	return
}

// 确认订单已打款
func (s *Service) Confirm(c context.Context, req *api.ConfirmReq) (resp *api.EmptyReq, err error) {
	if req.GetOrderId() == "" {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	req.UserId, err = strconv.ParseInt(c.Value("userID").(string), 10, 64)
	if err != nil {
		return
	}
	order, err := s.dao.Confirm(req)

	if err != nil {
		return
	}
	// 发消息通知 取反用户
	var resId int64
	if order.SponsorID == req.UserId {
		resId = order.TraderID
	} else {
		resId = order.SponsorID
	}
	if mh := websocks.MessagerInstance.(*websocks.Protobuf).GetMethod(&websocket.OrderDetail{}); mh != nil {
		if client, ok := websocks.WebServerInstance.Users[resId]; ok {
			go mh([]interface{}{&websocket.OrderDetail{OrderId: order.OrderID}, client})
		}
	}
	return
}

// 完成订单
func (s *Service) Complete(c context.Context, req *api.CompleteReq) (resp *api.EmptyReq, err error) {
	if req.GetOrderId() == "" {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	req.UserId, err = strconv.ParseInt(c.Value("userID").(string), 10, 64)
	if err != nil {
		return
	}
	order, err := s.dao.Complete(req)

	if err != nil {
		return
	}
	// 发消息通知 取反用户
	var resId int64
	if order.SponsorID == req.UserId {
		resId = order.TraderID
	} else {
		resId = order.SponsorID
	}
	if mh := websocks.MessagerInstance.(*websocks.Protobuf).GetMethod(&websocket.OrderDetail{}); mh != nil {
		if client, ok := websocks.WebServerInstance.Users[resId]; ok {
			go mh([]interface{}{&websocket.OrderDetail{OrderId: order.OrderID}, client})
		}
	}
	return
}

// 完成订单
func (s *Service) InternalComplete(c context.Context, req *api.CompleteReq) (resp *api.EmptyReq, err error) {
	if req.GetOrderId() == "" || req.GetUserId() == 0 {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	_, err = s.dao.Complete(req)
	if err != nil {
		return
	}

	// // 发消息通知 取反用户
	// var resId int64
	// if order.SponsorID == req.UserId {
	// 	resId = order.TraderID
	// } else {
	// 	resId = order.SponsorID
	// }
	// if mh := websocks.MessagerInstance.(*websocks.Protobuf).GetMethod(&websocket.OrderDetail{}); mh != nil {
	// 	if client, ok := websocks.WebServerInstance.Users[resId]; ok {
	// 		go mh([]interface{}{&websocket.OrderDetail{OrderId: order.OrderID}, client})
	// 	}
	// }
	return
}

// 订单信息
func (s *Service) OrderInfo(c context.Context, req *api.OrderDetailReq) (resp *api.OrderDetailResp, err error) {

	order, err := s.dao.OrderDetail(req.GetOrderId())
	if err != nil {
		return
	}

	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	if err != nil {
		return
	}

	if userId != order.TraderID && userId != order.SponsorID {
		err = ecode.Error(ecode.RequestErr, "不是你的订单，无权操作")
		return
	}

	var pay_user_id int64
	if order.AdvertiseType == paramers.OTC_SELL_TYPE {
		pay_user_id = order.SponsorID
	} else {
		pay_user_id = order.TraderID
	}

	pay_list := make([]*api.PayInfo, 0)
	list, err := s.dao.GetMemberProfiles(pay_user_id)
	if err == nil {
		for _, v := range list {
			pay_list = append(pay_list, &api.PayInfo{
				PayWay:      v.Type,
				PayNum:      v.BoundNum,
				PayUserName: v.RealName,
				PayImage:    v.FontImage,
			})
		}
	}

	timeout := (paramers.CONFIRM_ORDER_TIMEOUT * 60) - (time.Now().Unix() - order.CreateAt)
	resp = &api.OrderDetailResp{
		Status:        order.Status,
		OrderId:       order.OrderID,
		TraderId:      order.TraderID,
		AdvertiseType: order.AdvertiseType,
		Amount:        strconv.FormatFloat(order.Amount, 'f', -1, 64),
		PayImage:      order.PayImage,
		PayInfo:       pay_list,
		PayTime:       order.PayTime,
		Timeout:       timeout,
		UserName:      "12",
		Price:         strconv.FormatFloat(order.Price, 'f', -1, 64),
		Total:         strconv.FormatFloat(order.Total, 'f', -1, 64),
		AdvertiseId:   order.AdvertiseId,
	}
	return
}

// 取消订单
func (s *Service) OrderCancel(c context.Context, req *api.OrderCancelReq) (resp *api.EmptyReq, err error) {

	if req.GetOrderId() == "" {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}

	err = s.dao.Cancel(req.GetOrderId())
	if err != nil {
		return
	}

	return
}

// 订单列表
func (s *Service) OrderList(ctx context.Context, req *api.AuthTreadReq) (resp *api.EmptyReq, err error) {
	return
}
