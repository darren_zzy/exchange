package service

import (
	"context"
	"git.digittraders.com/exchange/api"
)

// 信息 合并接口
func (s *Service) Sync(c context.Context, req *api.EmptyReq) (resp *api.SyncResp, err error) {
	return s.dao.Sync()
}
