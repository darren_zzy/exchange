package service

import (
	"context"
	"encoding/json"
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/pkg/lib"
	"github.com/go-kratos/kratos/pkg/ecode"
	"strconv"
	"strings"
)

// 接口简要介绍
func (s *Service) Login(c context.Context, req *api.LoginReq) (resp *api.LoginResp, err error) {
	req.Email = strings.TrimSpace(req.Email)
	if req.Mobile == "" && req.Email == "" {
		err = ecode.Error(ecode.RequestErr, "mobile or email not exist")
		return
	}
	if req.Password == "" {
		err = ecode.Error(ecode.RequestErr, "password is empty")
		return
	}
	data, err := s.dao.Login(req)
	if err != nil {
		// err = ecode.Error(ecode.ServerErr, "Authorize faild")
		return
	}
	// 登录成功 鉴权
	resp = new(api.LoginResp)
	resp.UserId = data.ID
	resp.Token = data.Token
	return resp, s.dao.SetAuthorize(c, data)
}

func (s *Service) Register(c context.Context, req *api.RegisterReq) (resp *api.RegisterResp, err error) {
	if req.Mobile == "" || req.Country == 0 || req.Email == "" || req.Username == "" || req.Password == "" {

		str, _ := json.Marshal(req)
		err = ecode.Error(ecode.RequestErr, "缺省参数: "+string(str))
		return
	}

	if !lib.VerifyPhone(req.GetMobile()) {
		err = ecode.Error(ecode.RequestErr, "手机号格式不合法")
		return
	}
	if !lib.VerifyEmail(req.GetEmail()) {
		err = ecode.Error(ecode.RequestErr, "邮箱格式不合法")
		return
	}

	if err = s.dao.Register(req); err != nil {
		return
	}
	return
}

func (s *Service) CountryList(c context.Context, req *api.EmptyReq) (resp *api.CountryListResp, err error) {
	temps, err := s.dao.CountryList()
	resp = &api.CountryListResp{}
	resp.List = make([]*api.Country, 0)
	if err == nil {
		for _, v := range temps {
			resp.List = append(resp.List, &api.Country{
				Id:       v.ID,
				Name:     v.Name,
				LangFlag: v.Currency,
				Code:     v.AreaCode,
				Lang:     v.Language,
				Money:    v.Currency,
			})
		}
	}
	return
}

func (s *Service) AuthPing(c context.Context, req *api.EmptyReq) (resp *api.EmptyReq, err error) {
	return
}

// 法币种列表
func (s *Service) CurrencyList(c context.Context, req *api.CurrencyListReq) (resp *api.CurrencyListResp, err error) {
	list, err := s.dao.CurrencyList()
	if err != nil {
		return
	}
	resp = new(api.CurrencyListResp)
	resp.List = make([]*api.Currency, 0)
	for _, v := range list {
		resp.List = append(resp.List, &api.Currency{
			Id:            v.ID,
			Symbol:        v.Symbol,
			UsdToThisRate: strconv.FormatFloat(v.UsdToThisRate, 'f', -1, 64),
			Precision:     v.Precision,
			FullName:      v.FullName,
			Code:          v.Code,
		})
	}

	return
}

// 修改个人中心数据
func (s *Service) UpdateMyCenter(c context.Context, req *api.UpdateMyCenterReq) (resp *api.EmptyReq, err error) {
	req.UserId = c.Value("userID").(string)
	// 修改内容： 1 改手机号 2改邮箱 3 改登录密码 4 改交易密码
	switch req.GetType() {
	case 1:
		if !lib.VerifyPhone(req.GetPhone()) || !lib.VerifyPhone(req.GetOldPhone()) {
			err = ecode.Error(ecode.RequestErr, "手机号格式不合法")
			return
		}
		err = s.dao.UpdatePhone(req)
	case 2:
		if !lib.VerifyEmail(req.GetEmail()) {
			err = ecode.Error(ecode.RequestErr, "邮箱格式不合法")
			return
		}
		err = s.dao.UpdateEmail(req)
	case 3:
		if req.GetLoginPass() == "" || req.GetOldLoginPass() == "" {
			err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
			return
		}
		err = s.dao.UpdateLoginPass(req)

	case 4:
		if req.GetTreadPass() == "" {
			err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
			return
		}
		err = s.dao.UpdateTreadPass(req)
	default:
		err = ecode.Error(ecode.ServerErr, "类型参数无效，请根据文档传参")
	}
	return
}

// 验证资金密码
func (s *Service) AuthTread(c context.Context, req *api.AuthTreadReq) (resp *api.EmptyReq, err error) {
	user_id := c.Value("userID").(string)
	err = s.dao.AuthTreadPass(req.Pass, user_id)
	if err != nil {
		return
	}
	return
}

// 个人中心
func (s *Service) MyCenter(c context.Context, req *api.EmptyReq) (resp *api.MyCenterResp, err error) {
	resp = new(api.MyCenterResp)
	var user_id int64
	if user_id, err = strconv.ParseInt(c.Value("userID").(string), 10, 64); err != nil {
		return
	}
	user, err := s.dao.GetMember(&api.MemberReq{UserId: user_id})
	if err != nil || user.ID == 0 {
		return
	}

	resp.CenterData = &api.CenterData{
		Email:             user.Email,
		EmailValidateFlag: true,
		LoginPwdExist:     true,
		LoginPwdLevel:     1,
		Phone:             user.Mobile,
		TradeFrequency:    "****",
		TradeGoogleSwitch: false,
		TradePwdExist:     true,
		TradeSmsSwitch:    true,
	}

	capital, err := s.dao.GetMemberCapital(c.Value("userID").(string))
	if err == nil {
		resp.CenterData.BoundNum = capital.BoundNum
		resp.CenterData.RealName = capital.RealName
		resp.CenterData.BackImage = capital.BackImage
		resp.CenterData.FrontImage = capital.FontImage
		resp.CenterData.HandImage = capital.HandImage
	}

	profiles, err := s.dao.GetMemberProfiles(user_id)
	if err == nil {
		for _, v := range profiles {
			switch v.Type {
			case 1:
				resp.CenterData.AlipayImage = v.FontImage
				resp.CenterData.AlipayNum = v.BoundNum
				resp.Payway = append(resp.Payway, 1)
			case 2:
				resp.CenterData.WechatImage = v.FontImage
				resp.CenterData.WechatNum = v.BoundNum
				resp.Payway = append(resp.Payway, 2)
			case 3:
				resp.CenterData.BankImage = v.FontImage
				resp.CenterData.BankNum = v.BoundNum
				resp.CenterData.BankCardAuth = v.BankCardAuth
				resp.CenterData.BankPhone = v.Phone

				resp.Payway = append(resp.Payway, 3)
			}
		}
	}

	resp.Withdraw = &api.Withdraw{
		Amounts:            []string{"12222"},
		Balance:            "999",
		CanWithdrawAccount: "999",
		CanWithdrawAddress: "999",
		DailyMax:           "999",
		DailyTotal:         "999",
		DailyUsed:          "999",
		GoogleVerification: "999",
		LastAddress:        "999",
		Level:              "999",
		PercentageFee:      "999",
		PercentageFeeDesc:  "999",
	}
	return
}
