package service

import (
	"context"
	"fmt"
	"git.digittraders.com/exchange/api"
	"github.com/go-kratos/kratos/pkg/ecode"
	"strconv"
)

//  账单明细
// `midware:"auth" method:"get"`
func (s *Service) BillList(c context.Context, req *api.BillListReq) (resp *api.BillListResp, err error) {

	return
}

//   充币
// `midware:"auth" method:"post"`
func (s *Service) Deposit(c context.Context, req *api.DepositReq) (resp *api.DepositResp, err error) {

	return
}

//   添加充币地址
// `midware:"auth" method:"post"`
func (s *Service) AddDeposit(c context.Context, req *api.DepositMs) (resp *api.EmptyReq, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	_, _, err = s.dao.AddDeposit(req, userId)
	return
}

//    提币
// `midware:"auth" method:"post"`
func (s *Service) Withdrawal(c context.Context, req *api.WithdrawalReq) (resp *api.WithdrawalResp, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	err = s.dao.Withdrawal(req, userId)
	return
}

//    添加提币地址
// `midware:"auth" method:"post"`
func (s *Service) AddWithdrawal(c context.Context, req *api.DepositMs) (resp *api.EmptyReq, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)

	err = s.dao.AddWithdrawal(req, userId)
	return
}

//   法币资产
func (s *Service) AccountBalance(c context.Context, req *api.AccountBalanceReq) (resp *api.AccountBalanceResp, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	resp = new(api.AccountBalanceResp)
	resp.List = s.dao.BalanceList(userId)
	// resp.CanWithdrawalRmb = s.dao.GetRmbBalance()
	return
}

//    账单明细
func (s *Service) DealHistory(c context.Context, req *api.DealHistoryReq) (resp *api.DealHistoryResp, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	list, err := s.dao.DealHistory(req, userId)
	if err != nil {
		return
	} else if len(list) == 0 {
		err = ecode.Error(ecode.NothingFound, "历史记录为空")
		return
	}
	resp = new(api.DealHistoryResp)
	resp.List = make([]*api.DealHistory, 0)
	for _, v := range list {
		resp.List = append(resp.List, &api.DealHistory{
			Id:       v.ID,
			Amount:   strconv.FormatFloat(v.Amount, 'f', -1, 64),
			Type:     v.Type,
			CoinId:   v.CoinID,
			DealTime: v.CreateTime.Format("2006-01-02 15:04:05"),
			Balance:  strconv.FormatFloat(v.HistoryAmount, 'f', -1, 64),
		})
	}
	return
}

// 获取 充提 币种 地址
func (s *Service) GetWithdrawal(c context.Context, req *api.GetWithdrawalReq) (resp *api.GetWithdrawalResp, err error) {
	if req.GetType() == 0 {
		err = ecode.Error(ecode.RequestErr, fmt.Sprintf("%s ", ecode.ErrParamMsg))
		return
	}

	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)

	list := s.dao.UserWithdrawal(userId, req)
	resp = new(api.GetWithdrawalResp)
	resp.List = make([]*api.DepositMs, 0)

	var tag int
	for _, v := range list {
		resp.List = append(resp.List, &api.DepositMs{
			CoinId:   v.CoinID,
			Address:  v.Address,
			WalletId: v.ID,
		})
		if req.CoinId == v.CoinID {
			tag++
		}
	}

	// 当没有充币地址时去申请
	if req.CoinId > 0 && tag == 0 && req.Type == 1 {
		var address string
		var wallet_id int64
		wallet_id, address, err = s.dao.AddDeposit(&api.DepositMs{CoinId: req.CoinId}, userId)
		if err != nil {
			return
		}
		resp.List = append(resp.List, &api.DepositMs{
			CoinId:   req.CoinId,
			Address:  address,
			WalletId: wallet_id,
		})
	}

	return
}

// 设置默认银行卡
func (s *Service) DefaultBank(c context.Context, req *api.BankReq) (resp *api.Banks, err error) {
	// userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	//
	// resp = new(api.Banks)
	//
	// resp, err := s.dao.DefaultBank(req.Id, userId)

	return
}

// 删除银行卡
func (s *Service) DelBank(c context.Context, req *api.BankReq) (resp *api.EmptyReq, err error) {
	err = s.dao.DelMemberProfiles(req.Id)
	return
}
