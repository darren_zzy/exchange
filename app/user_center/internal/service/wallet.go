package service

import (
	"context"
	"git.digittraders.com/exchange/api"
	"strconv"
)

//    提现人民币
// `midware:"auth" method:"post"`
func (s *Service) WithdrawalRmb(c context.Context, req *api.WithdrawalRmbReq) (resp *api.EmptyReq, err error) {
	return
}

//    转账
// `midware:"auth" method:"post"`
func (s *Service) Transaction(c context.Context, req *api.TransactionReq) (resp *api.EmptyReq, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)
	err = s.dao.Transaction(req, userId)
	return
}

//    兑汇
// `midware:"auth" method:"post"`
func (s *Service) Transfers(c context.Context, req *api.TransfersReq) (resp *api.EmptyReq, err error) {
	return
}

//    兑汇账单明细
// `midware:"auth" method:"get"`
func (s *Service) TransfersHistory(c context.Context, req *api.DealHistoryReq) (resp *api.DealHistoryResp, err error) {
	return
}

//    银行卡列表
func (s *Service) BankList(c context.Context, req *api.EmptyReq) (resp *api.BankListResp, err error) {
	userId, err := strconv.ParseInt(c.Value("userID").(string), 10, 64)

	resp = new(api.BankListResp)
	resp.List = make([]*api.Banks, 0)
	profiles, err := s.dao.GetMemberProfiles(userId)

	if err == nil {
		for _, v := range profiles {
			switch v.Type {
			// case 1:
			// 	resp.CenterData.AlipayImage = v.FontImage
			// 	resp.CenterData.AlipayNum = v.BoundNum
			// 	resp.Payway = append(resp.Payway, 1)
			// case 2:
			// 	resp.CenterData.WechatImage = v.FontImage
			// 	resp.CenterData.WechatNum = v.BoundNum
			// 	resp.Payway = append(resp.Payway, 2)
			case 3:
				resp.List = append(resp.List, &api.Banks{
					BankName:     v.BankName,
					Id:           v.ID,
					Image:        v.FontImage,
					BoundNum:     v.BoundNum,
					BankCardAuth: v.BankCardAuth,
					Phone:        v.Phone,
				})

			}
		}
	}
	return
}
