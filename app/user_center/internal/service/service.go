package service

import (
	"context"
	"github.com/go-kratos/kratos/pkg/ecode"
	bm "github.com/go-kratos/kratos/pkg/net/http/blademaster"

	pb "git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/internal/dao"
	"github.com/go-kratos/kratos/pkg/conf/paladin"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/wire"
)

var Provider = wire.NewSet(New, wire.Bind(new(pb.UserCenterServer), new(*Service)))

// Service service.
type Service struct {
	ac  *paladin.Map
	dao dao.Dao
}

// New new a service and return.
func New(d dao.Dao) (s *Service, cf func(), err error) {
	s = &Service{
		ac:  &paladin.TOML{},
		dao: d,
	}
	cf = s.Close
	err = paladin.Watch("application.toml", s.ac)
	return
}

// Ping ping the resource.
func (s *Service) Ping(ctx context.Context, e *empty.Empty) (*empty.Empty, error) {
	return &empty.Empty{}, s.dao.Ping(ctx)
}

// Close close the resource.
func (s *Service) Close() {
}

func (s *Service) AuthUser(ctx *bm.Context) error {
	if ctx.Request.Header.Get("token") == "" || ctx.Request.Header.Get("userId") == "" {
		return ecode.Error(ecode.ServerErr, "not found userId or token on header!")

	}
	if !s.dao.AuthUser(ctx) {
		return ecode.Error(ecode.ServerErr, "Authorize faild")
	}
	return nil
}
