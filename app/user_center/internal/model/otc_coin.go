package model

type OtcCoin struct {
	BuyMinAmount  float64 `gorm:"column:buy_min_amount" json:"buy_min_amount"`
	CoinID        int64   `gorm:"column:coin_id" json:"coin_id"`
	ID            int64   `gorm:"column:id;primary_key" json:"id;primary_key"`
	SellMinAmount float64 `gorm:"column:sell_min_amount" json:"sell_min_amount"`
	Sort          int     `gorm:"column:sort" json:"sort"`
	Status        int     `gorm:"column:status" json:"status"`
	Precision     int     `gorm:"column:precision" json:"precision"`
	Symbol        string  `gorm:"column:symbol" json:"symbol"`

	BuyRate      float64 `gorm:"column:buy_rate" json:"buy_rate"`
	SellRate     float64 `gorm:"column:sell_rate" json:"sell_rate"`
	ExchangeRate float64 `gorm:"column:exchange_rate" json:"exchange_rate"`
}

// TableName sets the insert table name for this struct type
func (o *OtcCoin) TableName() string {
	return "otc_coin"
}
