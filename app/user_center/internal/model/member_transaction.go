package model

import "time"

type MemberTransaction struct {
	Amount        float64   `gorm:"column:amount" json:"amount"`
	HistoryAmount float64   `gorm:"column:history_amount" json:"history_amount"`
	CoinID        int64     `gorm:"column:coin_id" json:"coin_id"`
	CreateTime    time.Time `gorm:"column:create_time" json:"create_time"`
	Fee           float64   `gorm:"column:fee" json:"fee"`
	ID            int64     `gorm:"column:id;primary_key" json:"id;primary_key"`
	MemberID      int64     `gorm:"column:member_id" json:"member_id"`
	RealFee       float64   `gorm:"column:real_fee" json:"real_fee"`
	AccountId     int64     `gorm:"column:account_id" json:"account_id"`
	Remark        string    `gorm:"column:remark" json:"remark"`

	Type int64 `gorm:"column:type" json:"type"`
	// 交易类型:\n
	// 1  RECHARGE "充值"\n
	// 2 WITHDRAW "提现"\n
	// 3 TRANSFER_ACCOUNTS "转账"\n
	// 4 EXCHANGE "币币交易"\n
	// 5 OTC_BUY "法币买入"\n
	// 6 OTC_SELL "法币卖出"\n
	// 7 ACTIVITY_AWARD "活动奖励"\n
	// 8 PROMOTION_AWARD "推广奖励"\n
	// 9 DIVIDEND "分红"\n
	// 10 VOTE "投票"\n
	// 11 ADMIN_RECHARGE "人工充值"\n
	// 12 MATCH "配对"\n
	// 13 ACTIVITY_BUY "活动兑换"\n
	// 14 CTC_BUY "CTC 买入"\n
	// 15 CTC_SELL "CTC 卖出"\n
	// 16 RED_OUT "红包发出"\n
	// 17 RED_IN "红包领取"
}

// 用户交易
func (c *MemberTransaction) TableName() string {
	return "member_transaction"
}
