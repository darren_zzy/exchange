package model

import "time"

type OtcAppeal struct {
	AdvertiseID int64     `gorm:"column:advertise_id" json:"advertise_id"`
	AssociateID int64     `gorm:"column:associate_id" json:"associate_id"` // 被投诉人
	CreateAt    time.Time `gorm:"column:create_at" json:"create_at"`
	ID          int64     `gorm:"column:id;primary_key" json:"id;primary_key"`
	InitiatorID int64     `gorm:"column:initiator_id" json:"initiator_id"` // 发起人
	OrderID     string    `gorm:"column:order_id" json:"order_id"`
	Status      int64     `gorm:"column:status" json:"status"`
	Type        int64     `gorm:"column:type_id" json:"type_id"` // sell（ B申诉A为2 A申诉B为3 ）;  buy （B申诉A为4 A申诉B为1）
	UpdateAt    time.Time `gorm:"column:update_at" json:"update_at"`
}

// TableName sets the insert table name for this struct type
func (o *OtcAppeal) TableName() string {
	return "otc_appeal"
}
