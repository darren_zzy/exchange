package dao

import (
	"context"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/internal/model"
	"git.digittraders.com/exchange/paramers"
	"github.com/go-kratos/kratos/pkg/cache/redis"
	"github.com/go-kratos/kratos/pkg/ecode"
	"github.com/go-kratos/kratos/pkg/log"
	"github.com/google/uuid"
	"strconv"
	"time"
)

func (d *Dao) Login(req *api.LoginReq) (ct model.Member, err error) {
	db := d.orm.Table("member")
	if req.GetMobile() != "" {
		db = db.Where("mobile=?", req.Mobile)
	} else if req.GetEmail() != "" {
		db = db.Where("email=?", req.Email)
	} else {
		err = ecode.Error(ecode.RequestErr, ecode.ErrParamMsg)
		return
	}
	db.Take(&ct)
	// if err = db.First(&ct).Error; err != nil {
	// 	return ct, err
	// }
	md5Password := fmt.Sprintf("%x", md5.Sum([]byte(req.Password)))
	if ct.Password == "" || md5Password != ct.Password {
		err = ecode.Error(ecode.ServerErr, "password is wrong")
		return
	}
	return
}

func (d *Dao) Register(req *api.RegisterReq) (err error) {

Generate:
	uid := uuid.New().ID()
	exit := 0
	d.orm.Table("member").Where("uuid=?", uid).Count(&exit)
	if exit > 0 {
		goto Generate
	}

	var member model.Member
	tx := d.orm.Begin()
	md5Password := fmt.Sprintf("%x", md5.Sum([]byte(req.Password)))
	token := uuid.New().String()

	err = tx.Table(member.TableName()).
		Where("mobile=?", req.Mobile).
		FirstOrCreate(&model.Member{
			ID:       0, // 防止FirstOrCreate时主键重复
			Username: req.Username,
			Token:    token,
			Password: md5Password,
			CreateAt: time.Now(),
			UpdateAt: time.Now(),
			Email:    req.Email,
			Uuid:     int64(uid),
			Mobile:   req.Mobile,
			Country:  req.Country,
		}).Error
	if err != nil {
		tx.Rollback()
		return
	}
	if err = tx.Commit().Error; err != nil {
		tx.Rollback()
		return
	}

	return
}

func (d *Dao) CountryList() (list []model.Country, err error) {
	if err = d.orm.Table("country").
		Order("sort desc").
		Find(&list).Error; err != nil {
		return
	}
	return
}

func (d *Dao) GetMember(req *api.MemberReq) (data *model.Member, err error) {
	if req.GetPhone() == "" && req.GetUserId() == 0 && req.GetUuid() == "" {
		err = ecode.Error(ecode.ServerErr, fmt.Sprintf("query is null : %+v", req))
		return
	}
	query := d.orm.Table("member")

	data = &model.Member{}

	if req.UserId > 0 {
		// 读缓存
		if userBytes, errs := redis.Bytes(d.redis.Kdo("GET", RKUser(req.UserId))); errs == nil {
			if err = json.Unmarshal(userBytes, &data); err == nil {
				return
			}
		}
		query = query.Where("id=?", req.UserId)
	} else if req.Phone != "" {
		query = query.Where("mobile=?", req.Phone)
	} else if req.Uuid != "" {
		query = query.Where("uuid=?", req.Uuid)

	}
	// 查库
	err = query.
		Last(data).Error
	if err != nil {
		if b, err := json.Marshal(&data); err == nil {
			// 防止穿透
			d.redis.Kdo("SET", RKUser(req.GetUserId()), string(b), "EX", 5)
		}
		return
	}
	log.Info("%+v", data)
	if b, err := json.Marshal(&data); err == nil {
		d.redis.Kdo("SET", RKUser(data.ID), string(b), "EX", 18000)
	}
	return
}

// 获取用户交易信息
func (d *Dao) GetUserTradingInfo(userId int64) (resp *api.TradingInfo, err error) {
	resp = new(api.TradingInfo)

	key := RKTradingUser(userId)
	data, err := redis.Bytes(d.redis.Kdo("get", key))
	if err == nil {
		if err = json.Unmarshal(data, resp); err == nil {
			return
		}
	}

	var list []model.OtcOrder
	var complate_order, total int

	err = d.orm.Table("otc_order").
		Where("sponsor_id=?", userId).
		Find(&list).Error
	total = len(list)
	if err != nil {
		return
	}

	// 当没有订单时直接返回
	if total == 0 {
		resp.Closing = "0%"
		resp.Total = "0"
		if bates, err1 := json.Marshal(resp); err1 == nil {
			// 防止穿透
			d.redis.Kdo("SET", key, string(bates), "EX", 10)
			return
		}
	}

	for _, v := range list {
		// 1 开启 2 结束 3冻结
		if v.Status == 2 {
			complate_order++
		}
	}

	resp.Closing = fmt.Sprintf("%d", (complate_order/total)*100) + "%"
	resp.Total = fmt.Sprintf("%d", total)

	if bates, err := json.Marshal(resp); err == nil {
		d.redis.Kdo("SET", key, string(bates), "EX", 18600)
	}
	return
}

func (d *Dao) OtcAdvertiseList(req *api.AdvertiseListReq) (list []*model.OtcAdvertise, count int64, err error) {
	query := d.orm.Table("otc_advertise")

	if req.GetCoin() != "" {
		coinId, ok := d.dataCfg.coinNames[req.GetCoin()]
		if !ok {
			err = ecode.Error(ecode.ServerErr, fmt.Sprintf("not found this coin: %s", req.GetCoin()))
		}
		query = query.Where("coin_id = ?", coinId)
	}

	if req.GetType() > 0 {
		query = query.Where("type = ?", req.GetType())
	}

	if req.GetRange() > 0 {
		query = query.Where("max_limit >=?", req.GetRange()).Where("min_limit <=?", req.GetRange())
	}

	if req.GetUserId() > 0 {
		query = query.Where("owner_id =?", req.GetUserId())
	}

	// 上架广告
	query = query.Where("status =1")
	// 未在交易中
	query = query.Where("is_trade =1")
	query.Count(&count)
	if count == 0 {
		return
	}
	// 偏移id
	offset := req.GetPage() - 1
	if offset > 0 {
		// query = query.Where("id >?", req.GetPage())
		query = query.Offset(offset * 20)
	}
	err = query.Limit(20).Order("id asc").Find(&list).Error
	if err != nil {
		return
	}
	return
}
func (d *Dao) GetAdvertise(id int64) (data model.OtcAdvertise, err error) {
	err = d.orm.Table("otc_advertise").Take(&data, "id=?", id).Error
	return
}

func (d *Dao) DelAdvertise(id int64) (err error) {
	data, err := d.GetAdvertise(id)
	if err != nil {
		return
	}
	// 下架
	data.Status = 2
	d.orm.Model(data).Save(&data)
	return
}

func (d *Dao) AddAdvertise(c context.Context, req *api.AddAdvertiseReq) (err error) {
	var data model.OtcAdvertise
	TotalAmount, err := strconv.ParseFloat(req.GetTotalAmount(), 8)
	if err != nil {
		return
	}
	MaxLimit, err := strconv.ParseFloat(req.GetMaxLimit(), 8)
	if err != nil {
		return
	}
	MinLimit, err := strconv.ParseFloat(req.GetMinLimit(), 8)
	if err != nil {
		return
	}
	price, err := strconv.ParseFloat(req.GetPrice(), 8)
	if err != nil {
		return
	}

	if TotalAmount*price > MaxLimit {
		// err = ecode.Error(ecode.ServerErr,"")
		// return
		MaxLimit = TotalAmount * price
	}

	data.CurrencyId = 1
	data.Price = price
	data.Type = req.GetType()
	data.CoinID = req.GetCoinId()
	data.TotalAmount = TotalAmount
	data.RemainAmount = TotalAmount
	data.MaxLimit = MaxLimit
	data.Status = 1
	data.IsTrade = paramers.NOT_TREADING
	data.MinLimit = MinLimit
	data.Remark = req.GetRemark()

	if data.OwnerID, err = strconv.ParseInt(c.Value("userID").(string), 10, 64); err != nil {
		return
	}
	data.PayMode = req.GetPayWay()
	data.AutoReply = req.GetAutoReply()
	data.Timeout = req.GetTimeout() * 60
	data.CreateAt = time.Now()

	if err = d.orm.Save(&data).Error; err != nil {
		err = ecode.Error(ecode.ServerErr, err.Error())
		return
	}

	return
}

func (d *Dao) PayWayList() (list []*model.PayWay, err error) {
	err = d.orm.Table("pay_way").Where("status=1").Find(&list).Error
	return
}

func (d *Dao) Sync() (resp *api.SyncResp, err error) {
	resp = new(api.SyncResp)
	resp.PayWay = make([]*api.PayWay, 0)
	resp.Fiats = make([]*api.Fiats, 0)
	resp.OtcCoins = make([]*api.OtcCoins, 0)

	payWays, err := d.PayWayList()
	if err != nil {
		return
	}
	for _, v := range payWays {
		resp.PayWay = append(resp.PayWay, &api.PayWay{
			Id:   v.ID,
			Name: v.EnName,
		})
	}
	coins, err := d.LoadCoins()
	if err != nil {
		return
	}
	for _, v := range coins {
		resp.Fiats = append(resp.Fiats, &api.Fiats{
			Id:   v.ID,
			Name: v.Symbol,
		})
	}

	otc_coins, err := d.LoadOtcCoins()
	if err != nil {
		return
	}
	for _, v := range otc_coins {
		resp.OtcCoins = append(resp.OtcCoins, &api.OtcCoins{
			OtcCoinId:     v.ID,
			BuyRate:       v.BuyRate,
			SellRate:      v.SellRate,
			ExchangeRate:  v.ExchangeRate,
			CoinId:        v.CoinID,
			BuyMinAmount:  v.BuyMinAmount,
			SellMinAmount: v.SellMinAmount,
		})
	}
	return
}

// 添加支付方式
func (d *Dao) AddPayWay(req *api.AddPayWayReq) (err error) {
	data := model.MemberProfiles{
		UserID:       req.GetUserId(),
		FontImage:    req.GetFrontImage(),
		BankCardAuth: req.GetBankCardAuth(),
		BoundNum:     req.GetBoundNum(),
		RealName:     req.GetRealName(),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		BankName:     req.GetBankName(),
		Phone:        req.GetPhone(),
		Status:       paramers.PROFILE_OK,
		Type:         req.GetTypeId(),
	}
	// err = d.orm.Table("member_profiles").Where("user_id=?", req.GetUserId()).Where("type=?", req.GetTypeId()).Create(&data).Error
	err = d.orm.Table("member_profiles").Create(&data).Error
	return
}

// 实名认证
func (d *Dao) RealIdentity(req *api.RealIdentityReq) (err error) {
	data := model.MemberCapital{
		UserID:    req.GetUserId(),
		CreateAt:  time.Now(),
		Type:      req.GetType(),
		BackImage: req.GetBackImage(),
		BoundNum:  req.GetBoundNum(),
		FontImage: req.GetFrontImage(),
		HandImage: req.GetHandImage(),
		RealName:  req.GetRealName(),
	}
	err = d.orm.Table("member_capital").Where("user_id=?", req.GetUserId()).FirstOrCreate(&data).Error
	return
}

// 是否支付认证
func (d *Dao) HasQuality(c context.Context) (has bool) {
	userId := c.Value("userID").(string)
	var data model.MemberProfiles
	if err := d.orm.Table("member_profiles").
		Where("user_id=?", userId).Take(&data).Error; err != nil {
		log.Error("核验支付认证错误：", err.Error())
		return false
	}
	if data.ID > 0 {
		return true
	}
	return false
}

// 是否已做实名认证
func (d *Dao) HasRealName(c context.Context) (has bool) {
	userId := c.Value("userID").(string)
	data, err := d.GetMemberCapital(userId)
	if err != nil {
		log.Error("核验实名制错误：", err.Error())
		return
	}
	if data.ID > 0 {
		return true
	}
	return false
}

// 验证交易密码
func (d *Dao) AuthTreadPass(pass, user_id string) (err error) {
	data, err := d.GetMemberCapital(user_id)
	if err != nil {
		return
	}
	if data.Password != pass {
		err = ecode.Error(ecode.ServerErr, "资金密码错误")
		return
	}
	return
}

func (d *Dao) UpdatePhone(req *api.UpdateMyCenterReq) (err error) {
	var data model.Member
	err = d.orm.Table("member").
		Where("id=?", req.GetUserId()).
		Where("mobile=?", req.GetOldPhone()).
		Take(&data).Error
	if err != nil {
		err = ecode.Error(ecode.NothingFound, ecode.ErrParamMsg)
		return
	}
	data.Mobile = req.GetPhone()
	err = d.orm.Table("member").Save(&data).Error
	user_id, err := strconv.ParseInt(req.UserId, 10, 64)
	d.DelMemberCache(user_id)
	return
}

func (d *Dao) UpdateEmail(req *api.UpdateMyCenterReq) (err error) {
	var data model.Member
	err = d.orm.Table("member").
		Where("id=?", req.GetUserId()).
		Take(&data).Error
	if err != nil {
		err = ecode.Error(ecode.NothingFound, ecode.ErrParamMsg)
		return
	}
	data.Email = req.GetEmail()
	err = d.orm.Table("member").Save(&data).Error
	user_id, err := strconv.ParseInt(req.UserId, 10, 64)
	d.DelMemberCache(user_id)
	return
}

func (d *Dao) GetMemberCapital(user_id string) (data model.MemberCapital, err error) {
	err = d.orm.Table("member_capital").
		Where("user_id=?", user_id).
		Take(&data).Error
	if err != nil {
		err = ecode.Error(ecode.NothingFound, ecode.ErrParamMsg)
		return
	}
	return
}

func (d *Dao) GetMemberProfiles(user_id int64) (list []*model.MemberProfiles, err error) {
	err = d.orm.Table("member_profiles").
		Where("user_id=?", user_id).
		Where("status=?", paramers.PROFILE_OK).
		Find(&list).Error
	if err != nil {
		err = ecode.Error(ecode.NothingFound, ecode.ErrParamMsg)
		return
	}
	return
}

func (d *Dao) DelMemberProfiles(id int64) (err error) {
	err = d.orm.Table("member_profiles").
		Where("id=?", id).
		Delete(&model.MemberProfiles{}).Error
	if err != nil {
		err = ecode.Error(ecode.NothingFound, ecode.ErrParamMsg)
		return
	}
	return
}

func (d *Dao) DefaultBank(id, userId int64) (data model.MemberProfiles, err error) {

	var list []*model.MemberProfiles

	d.orm.Table("member_profiles").
		Where("user_id=?", userId).
		// Where("def=?", 1).
		Find(&list)

	if id == 0 {
		d.orm.Table("member_profiles").
			Where("user_id=?", userId).
			Where("def=?", 1).
			Take(&data)

	} else {
		err = d.orm.Table("member_profiles").
			Where("user_id=?", id).
			Update("def", 0).
			Error
		err = d.orm.Table("member_profiles").
			Where("id=?", id).
			Update("def", 1).Error
	}

	// Save(&model.MemberProfiles{}).Error
	if err != nil {
		err = ecode.Error(ecode.NothingFound, ecode.ErrParamMsg)
		return
	}
	return
}

func (d *Dao) UpdateTreadPass(req *api.UpdateMyCenterReq) (err error) {
	data, err := d.GetMemberCapital(req.GetUserId())
	if err != nil {
		return
	}
	data.Password = req.GetTreadPass()
	err = d.orm.Table("member_capital").Save(&data).Error
	return
}

func (d *Dao) UpdateLoginPass(req *api.UpdateMyCenterReq) (err error) {
	var data model.Member
	md5Password := fmt.Sprintf("%x", md5.Sum([]byte(req.OldLoginPass)))
	newMd5Password := fmt.Sprintf("%x", md5.Sum([]byte(req.LoginPass)))

	err = d.orm.Table("member").
		Where("id=?", req.GetUserId()).
		Where("password=?", md5Password).
		Take(&data).Error
	if err != nil {
		err = ecode.Error(ecode.NothingFound, ecode.ErrParamMsg)

		return
	}
	data.Password = newMd5Password
	err = d.orm.Table("member").Save(&data).Error
	user_id, err := strconv.ParseInt(req.UserId, 10, 64)
	d.DelMemberCache(user_id)
	return
}

// 法币列表
func (d *Dao) CurrencyList() (list []*model.Currency, err error) {
	err = d.orm.Table("currency").Where("status=1").Find(&list).Error
	if err != nil {
		return
	}
	return
}
