package dao

import (
	"fmt"
	"git.digittraders.com/exchange/pkg/database/orm"
	"github.com/go-kratos/kratos/pkg/conf/paladin"
	"log"
	"os"

	"github.com/jinzhu/gorm"
)

func NewORM() (db *gorm.DB, cf func(), err error) {
	var (
		cfg orm.Config
		ct  paladin.TOML
	)
	if err = paladin.Get("orm.toml").Unmarshal(&ct); err != nil {
		fmt.Printf("%v", err.Error())
		return
	}
	if err = ct.Get("Client").UnmarshalTOML(&cfg); err != nil {
		return
	}
	db = orm.NewMySQL(&cfg)

	// 开启orm日志记录
	db.LogMode(true)
	db.SetLogger(gorm.Logger{log.New(os.Stdout, "\r\n", 0)})
	cf = func() { db.Close() }
	return
}
