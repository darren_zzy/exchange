package dao

import (
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/internal/model"
	"git.digittraders.com/exchange/paramers"
	"github.com/go-kratos/kratos/pkg/ecode"
)

func (d *Dao) Transaction(req *api.TransactionReq, userId int64) (err error) {
	user := d.Balance(userId, req.CoinId)

	if user.RemainAmount < req.Amount {
		err = ecode.Error(ecode.ServerErr, "可用额度不足，请咨询后在转账")
		return
	}

	member, err := d.GetMember(&api.MemberReq{Uuid: req.Account})
	if err != nil {
		err = ecode.Error(ecode.ServerErr, "对方账户错误，请咨询后在转账")
		return
	}
	phone := member.Mobile

	if phone[len(phone)-4:] != req.Phone {
		err = ecode.Error(ecode.ServerErr, "对方手机号错误，请咨询后在转账")
		return
	}

	balance1, err := d.Disburse(userId, req.CoinId, paramers.TRANSFER_ACCOUNTS, req.Amount)

	if err != nil {
		return
	}

	balance2, err := d.IncomeCoin(member.ID, req.CoinId, paramers.TRANSFER_ACCOUNTS, req.Amount)

	if err != nil {
		return
	}

	go d.AddFlowRecord(&model.MemberTransaction{
		CoinID:        req.CoinId,
		Amount:        -req.Amount,
		HistoryAmount: balance1.Balance,
		Type:          paramers.TRANSFER_ACCOUNTS,
		MemberID:      balance1.MemberID,
		AccountId:     balance1.ID,
	})
	go d.AddFlowRecord(&model.MemberTransaction{
		CoinID:        req.CoinId,
		Amount:        req.Amount,
		HistoryAmount: balance2.Balance,
		Type:          paramers.TRANSFER_ACCOUNTS,
		MemberID:      balance2.MemberID,
		AccountId:     balance2.ID,
	})

	return
}

func (d *Dao) WithdrawalRmb(req *api.WithdrawalRmbReq, userId int64) (err error) {
	// var data model.MemberProfiles
	// err = d.orm.Table("member_profiles").
	// 	Where("status=?", paramers.PROFILE_OK).
	// 	Take(&data, "id=?", req.ProfilesId).Error
	// if err != nil {
	// 	err = ecode.Error(ecode.NothingFound, ecode.ErrNotFoundMsg)
	// 	return
	// }

	coin_id, ok := d.dataCfg.coinNames["CNY"]
	if ok {
		err = ecode.Error(ecode.ServerErr, "不存在 CNY 法币")
		return
	}

	req1 := new(api.OrderAdvertiseReq)
	req1.CoinId = coin_id
	req1.Amount = req.Amount
	req1.Type = paramers.OTC_BUY_TYPE
	// 创建订单  卖 usdt   买
	// 自动撮合广告单
	ads, err := d.QuickAdvertise(req1)

	if err != nil {
		return
	}
	req1.AdvertiseId = ads.ID
	// _, sponsor_id, err := d.CreateOtcOrder(req1, userId)

	// // 发消息
	// if mh := websocks.MessagerInstance.(*websocks.Protobuf).GetMethod(&websocket.OrderListReq{}); mh != nil {
	// 	if client, ok := websocks.WebServerInstance.Users[sponsor_id]; ok {
	// 		println("准备给客户端发消息 发起来！！！", sponsor_id)
	//
	// 		go mh([]interface{}{&websocket.OrderListReq{Status: 1}, client})
	// 	} else {
	// 		println("没找到用户", sponsor_id)
	// 	}
	// }

	return
}
