package dao

import (
	"fmt"
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/internal/model"
	"git.digittraders.com/exchange/paramers"
	"git.digittraders.com/exchange/pkg/lib"
	"github.com/go-kratos/kratos/pkg/ecode"
	"github.com/go-kratos/kratos/pkg/log"
	"strconv"
	"time"
)

// func (d *Dao) CreateOtcOrder(req *api.OrderAdvertiseReq, userId int64) (orderId string, sponsorId int64, err error) {
//
// 	amount, err := strconv.ParseFloat(req.GetAmount(), 8)
// 	if err != nil {
// 		return
// 	}
//
// 	var ads model.OtcAdvertise
// 	if err = d.orm.Table("otc_advertise").Take(&ads, "id=?", req.GetAdvertiseId()).Error; err != nil {
// 		return
// 	}
//
// 	if userId == ads.OwnerID {
// 		err = ecode.Error(ecode.ServerErr, "不能交易自己发布的广告")
// 		return
// 	}
//
// 	if paramers.IS_TREADING == ads.IsTrade {
// 		err = ecode.Error(ecode.ServerErr, "该广告正在交易中，请选择其他广告")
// 		return
// 	}
//
// 	var deal_user_id int64
// 	if ads.Type == paramers.OTC_SELL_TYPE {
// 		deal_user_id = ads.OwnerID
// 	} else {
// 		deal_user_id = userId
// 	}
// 	// 用户卖币类型 需要先判断资产 和 扣币流程
// 	if err = d.freeze(deal_user_id, ads.CoinID, amount); err != nil {
// 		return
// 	}
//
// 	ads.DealAmount = amount
// 	ads.RemainAmount = ads.TotalAmount - amount
// 	ads.IsTrade = paramers.IS_TREADING
// 	orderId = lib.GenerateID()
// 	sponsorId = ads.OwnerID
// 	// 降低最小额度
// 	if ads.RemainAmount < ads.MinLimit {
// 		ads.MinLimit = 0
// 	}
// 	order := &model.OtcOrder{
// 		OrderID:       orderId,
// 		AdvertiseType: ads.Type,
// 		AdvertiseId:   ads.ID,
// 		Amount:        amount,
// 		CreateAt:      time.Now().Unix(),
// 		Price:         ads.Price,
// 		Total:         ads.Price * amount,
// 		SponsorID:     ads.OwnerID,
// 		CoinId:        ads.CoinID,
// 		TraderID:      userId,
// 		Status:        paramers.ORDER_START,
// 	}
//
// 	err = d.transaction(order, &ads)
// 	go d.ConfirmTimeOut(orderId)
// 	// 下单失败 ，回款
// 	if err != nil {
// 		account := d.Balance(deal_user_id, ads.CoinID)
// 		account.DealAmount -= amount
// 		account.RemainAmount += amount
// 		account.UpdateAt = time.Now()
// 		err = d.orm.Table("member_account").
// 			Where("member_id=?", userId).
// 			Where("coin_id=?", ads.CoinID).
// 			Save(&account).Error
// 		if err != nil {
// 			err = ecode.Error(ecode.ServerErr, "还原钱包失败")
// 			return
// 		}
// 	}
// 	return
// }

func (d *Dao) BookOtcOrder(req *api.OrderAdvertiseReq, userId int64) (orderId string, sponsorId int64, err error) {

	amount, err := strconv.ParseFloat(req.GetAmount(), 8)
	if err != nil {
		return
	}

	// 自动撮合对方账户
	account2, err := d.MatchingAccount(req)
	if err != nil {
		return
	}
	// 撮合寻找 同币种 用户
	sponsor_id := account2.MemberID
	otc_coin, ok := d.dataCfg.otcCoinCfg[req.CoinId]
	if !ok {
		return
	}

	var deal_user_id int64
	var rate float64
	if req.Type == paramers.OTC_SELL_TYPE {
		deal_user_id = sponsor_id
		rate = otc_coin.SellRate
	} else {
		deal_user_id = userId
		rate = otc_coin.BuyRate
	}

	var account model.MemberAccount

	total := lib.FloatRound(amount/rate, otc_coin.Precision)

	// 买卖类型 需要先判断资产 和 扣币流程
	if account, err = d.freeze(deal_user_id, req.CoinId, total); err != nil {
		return
	}

	orderId = lib.GenerateID()

	account.MemberID = 1
	// 降低最小额度
	// if ads.RemainAmount < ads.MinLimit {
	// 	ads.MinLimit = 0
	// }
	order := &model.OtcOrder{
		OrderID:       orderId,
		AdvertiseType: req.Type,
		AdvertiseId:   0,
		Amount:        amount,
		CreateAt:      time.Now().Unix(),
		Price:         rate,
		Total:         total,
		SponsorID:     sponsor_id,
		CoinId:        req.CoinId,
		OtcCoinId:     otc_coin.ID,
		TraderID:      userId,
		Status:        paramers.ORDER_START,
	}

	err = d.orderTransaction(order)
	go d.ConfirmTimeOut(orderId)
	// 下单失败 ，回款
	if err != nil {
		account := d.Balance(userId, req.CoinId)
		account.DealAmount -= amount
		account.RemainAmount += amount
		account.UpdateAt = time.Now()
		err = d.orm.Table("member_account").
			Where("member_id=?", userId).
			Where("coin_id=?", req.CoinId).
			Save(&account).Error
		if err != nil {
			err = ecode.Error(ecode.ServerErr, "还原钱包失败")
			return
		}
	}
	return
}

// func(d *Dao)

// 确认已打款
func (d *Dao) Confirm(req *api.ConfirmReq) (order model.OtcOrder, err error) {
	order, err = d.OrderDetail(req.GetOrderId())
	if err != nil {
		return
	}
	if order.TraderID != req.UserId && order.SponsorID != req.UserId {
		err = ecode.Error(ecode.RequestErr, "不是你的订单，无权操作")
		return
	}
	if order.Status != paramers.ORDER_START {
		err = ecode.Error(ecode.RequestErr, fmt.Sprintf("当前订单状态异常 status: %d", order.Status))
		return
	}

	// 暂时支付信息存储用户支付 id
	order.PayInfo = strconv.FormatInt(req.GetPayWay(), 10)
	order.Status = paramers.ORDER_SERVICED
	order.PayTime = time.Now().Unix()
	order.PayImage = req.GetPayImage()

	d.orm.Table("otc_order").Save(&order)

	go d.CompleteTimeOut(order.OrderID)

	return
}

// 申诉订单
func (d *Dao) Complain(req *api.ComplainReq) (order model.OtcOrder, err error) {
	order, err = d.OrderDetail(req.GetOrderId())
	if err != nil {
		return
	}
	if order.TraderID != req.UserId && order.SponsorID != req.UserId {
		err = ecode.Error(ecode.RequestErr, "不是你的订单，无权操作")
		return
	}
	if order.Status != paramers.ORDER_START && order.Status != paramers.ORDER_SERVICED {
		err = ecode.Error(ecode.RequestErr, fmt.Sprintf("当前订单状态异常 status: %d", order.Status))
		return
	}
	order.Status = paramers.ORDER_COMPLAINING
	d.orm.Table("otc_order").Save(&order)
	return
}

// 完成订单
func (d *Dao) Complete(req *api.CompleteReq) (order model.OtcOrder, err error) {
	order, err = d.OrderDetail(req.GetOrderId())
	if err != nil {
		return
	}
	if order.TraderID != req.UserId && order.SponsorID != req.UserId {
		err = ecode.Error(ecode.RequestErr, "不是你的订单，无权操作")
		return
	}

	if order.Status != paramers.ORDER_SERVICED {
		err = ecode.Error(ecode.RequestErr, fmt.Sprintf("当前订单状态异常 status: %d", order.Status))
		return
	}

	var ads model.OtcAdvertise
	err = d.orm.Table("otc_advertise").Take(&ads, "id=?", order.AdvertiseId).Error
	if err != nil {
		return
	}

	var balance1, balance2 model.MemberAccount

	// 用户卖币类型 需要先判断资产 和 扣币流程 增币流程
	if ads.Type == paramers.OTC_SELL_TYPE {
		// 给发广告用户 扣币
		if balance1, err = d.DecreateCoin(order.SponsorID, order.CoinId, paramers.OTC_SELL, order.Amount); err != nil {
			return
		}
		// 给买币 用户充币
		if balance2, err = d.IncomeCoin(order.TraderID, order.CoinId, paramers.OTC_BUY, order.Amount); err != nil {
			return
		}

	} else {
		// 给发广告用户 充币
		if balance2, err = d.IncomeCoin(order.SponsorID, order.CoinId, paramers.OTC_BUY, order.Amount); err != nil {
			return
		}
		// 给卖币 用户扣币
		if balance1, err = d.DecreateCoin(order.TraderID, order.CoinId, paramers.OTC_SELL, order.Amount); err != nil {
			return
		}
	}

	go d.AddFlowRecord(&model.MemberTransaction{
		CoinID:        balance1.CoinID,
		Amount:        -order.Amount,
		HistoryAmount: balance1.Balance,
		Type:          paramers.TRANSFER_ACCOUNTS,
		MemberID:      balance1.MemberID,
		AccountId:     balance1.ID,
	})

	go d.AddFlowRecord(&model.MemberTransaction{
		CoinID:        balance2.CoinID,
		Amount:        order.Amount,
		HistoryAmount: balance2.Balance,
		Type:          paramers.TRANSFER_ACCOUNTS,
		MemberID:      balance2.MemberID,
		AccountId:     balance2.ID,
	})

	ads.RemainAmount = ads.RemainAmount - ads.DealAmount
	ads.MaxLimit = ads.RemainAmount * ads.Price
	ads.DealAmount = 0
	ads.IsTrade = paramers.NOT_TREADING

	order.Status = paramers.ORDER_COMPLETED
	order.FinishTime = time.Now().Unix()

	err = d.transaction(&order, &ads)
	return
}

func (d *Dao) OrderDetail(id string) (order model.OtcOrder, err error) {
	err = d.orm.Table("otc_order").Take(&order, "order_id=?", id).Error
	return
}

func (d *Dao) OrderList(status, userId string) (list []*model.OtcOrder, err error) {
	err = d.orm.Table("otc_order").
		Where("trader_id=? or sponsor_id=?", userId, userId).
		Where("status in (?,?,?)", 1, 2, 4).
		Order("create_at desc").
		Find(&list).Error
	return
}

// 后台处理申诉
func (d *Dao) OmComplain(orderId string) {

	var appeal model.OtcAppeal
	d.orm.Table("otc_appeal").Where("order_id=?", orderId).Take(&appeal)
	if appeal.ID == 0 {
		return
	}
	// banned为true 禁用账户

	_, err := d.OrderDetail(orderId)
	if err != nil {
		return
	}

	switch appeal.Type {
	case 1:
		// 代表该申诉者是广告发布者，并且是付款者   卖家associateId  buy

	case 2:
		// 代表该申诉者不是广告发布者，但是是付款者   卖家associateId sell

	case 3:
		// 代表该申诉者是广告发布者，但不是付款者   卖家initiatorId  sell

	case 4:
		// 代表该申诉者不是广告发布者，但不是付款者  卖家initiatorId buy

	default:

	}

}

// 超时未打款
func (d *Dao) ConfirmTimeOut(orderId string) {
	timer := time.NewTimer(paramers.CONFIRM_ORDER_TIMEOUT * time.Minute)
	select {
	case <-timer.C:
		order, err := d.OrderDetail(orderId)
		if err != nil {
			log.Info("查询订单失败 error：%s", err.Error())
			return
		}

		if order.Status < 2 {
			log.Info("订单超时未打款,订单关闭", order.OrderID)
			// var ads model.OtcAdvertise
			// var err error
			// if err = d.orm.Table("otc_advertise").Take(&ads, "id=?", order.AdvertiseId).Error; err != nil {
			// 	return
			// }
			// ads.RemainAmount += ads.DealAmount
			order.Status = paramers.ORDER_CLOSED
			order.FinishTime = time.Now().Unix()

			err = d.orderTransaction(&order)

		}
	}
}

// 确认订单超时未放币
func (d *Dao) CompleteTimeOut(orderId string) {
	timer := time.NewTimer(paramers.COMPLETE_ORDER_TIMEOUT * time.Minute)
	select {
	case <-timer.C:
		order, err := d.OrderDetail(orderId)
		if err != nil {
			log.Info("查询订单失败 error：%s", err.Error())
			return
		}

		if order.Status < 3 {
			log.Info("确认订单超时未放币", order.OrderID)
			var ads model.OtcAdvertise
			var err error
			if err = d.orm.Table("otc_advertise").Take(&ads, "id=?", order.AdvertiseId).Error; err != nil {
				return
			}
			ads.RemainAmount = 0
			ads.IsTrade = paramers.NOT_TREADING
			order.Status = paramers.ORDER_COMPLETED
			order.FinishTime = time.Now().Unix()
			err = d.transaction(&order, &ads)

		}
	}
}

// 取消订单
func (d *Dao) Cancel(orderId string) (err error) {
	order, err := d.OrderDetail(orderId)
	if err != nil {
		log.Info("查询订单失败 error：%s", err.Error())
		err = ecode.Error(ecode.ServerErr, "订单不存在")

		return
	}
	var ads model.OtcAdvertise
	if err = d.orm.Table("otc_advertise").Take(&ads, "id=?", order.AdvertiseId).Error; err != nil {
		err = ecode.Error(ecode.ServerErr, "广告不存在")

		return
	}

	// 根据订单状态 发送消息通知
	switch order.Status {
	case 1: // 	刚下单 取消
	case 4: // 	已申诉取消
	default:
		log.Info("订单不符合取消行为：%s   %d", orderId, order.Status)
		err = ecode.Error(ecode.ServerErr, "订单不符合取消行为")
		return
	}

	order.Status = paramers.ORDER_CLOSED
	order.FinishTime = time.Now().Unix()

	// 恢复广告可交易状态
	ads.IsTrade = paramers.NOT_TREADING
	ads.RemainAmount += ads.DealAmount
	ads.DealAmount = 0

	err = d.transaction(&order, &ads)

	// if err

	return
}

// 内部调用  事务提交
func (d *Dao) transaction(order *model.OtcOrder, ads *model.OtcAdvertise) (err error) {
	if ads == nil || order == nil {
		msg := fmt.Sprintf("广告| 订单 信息为空 ！ ")
		log.Error(msg)
		err = ecode.Error(ecode.ServerErr, msg)
		return
	}
	dbc := d.orm.Begin()
	if err = dbc.Table("otc_advertise").
		Save(&ads).Error; err != nil {
		log.Error("广告更新失败 err: %s， 当前广告可用量：%s", err.Error())
		dbc.Rollback()
		return
	}

	if err = dbc.Table("otc_order").Save(&order).Error; err != nil {
		log.Error("订单更新失败 err: %s， 当前订单状态：%d", err.Error(), order.Status)
		dbc.Rollback()
		return
	}
	if err = dbc.Commit().Error; err != nil {
		log.Error("事务提交失败 err: %s", err.Error())
		return
	}
	return
}

// 内部调用  事务提交
func (d *Dao) orderTransaction(order *model.OtcOrder) (err error) {
	if order == nil {
		msg := fmt.Sprintf("广告| 订单 信息为空 ！ ")
		log.Error(msg)
		err = ecode.Error(ecode.ServerErr, msg)
		return
	}
	dbc := d.orm.Begin()
	if err = dbc.Table("otc_order").Save(&order).Error; err != nil {
		log.Error("订单更新失败 err: %s， 当前订单状态：%d", err.Error(), order.Status)
		dbc.Rollback()
		return
	}
	if err = dbc.Commit().Error; err != nil {
		log.Error("事务提交失败 err: %s", err.Error())
		return
	}
	return
}

// 快捷获取可交易广告单
func (d *Dao) QuickAdvertise(req *api.OrderAdvertiseReq) (ads model.OtcAdvertise, err error) {
	amount, err := strconv.ParseFloat(req.Amount, 64)
	if err != nil {
		return
	}
	if err = d.orm.Table("otc_advertise").
		Where("type=?", req.Type).
		Where("is_trade=1").
		Where("remain_amount>=?", amount).
		Where("coin_id=?", req.CoinId).
		Last(&ads).Error; err != nil {
		return
	}

	return
}

// 快捷获取可交易广告单
func (d *Dao) MatchingAccount(req *api.OrderAdvertiseReq) (account model.MemberAccount, err error) {
	amount, err := strconv.ParseFloat(req.Amount, 64)
	if err != nil {
		return
	}
	if err = d.orm.Table("member_account").
		// Where("type=?", req.Type).
		// Where("is_trade=1").
		Where("remain_amount>=?", amount).
		Where("coin_id=?", req.CoinId).
		Where("status=1").
		Last(&account).Error; err != nil {
		return
	}

	return
}

func (d *Dao) SwitchOrder(status, userId int64) (err error) {
	var ads model.OtcAdvertise
	coinNames := d.dataCfg.coinNames
	coin_id, ok := coinNames["USDT"]
	if !ok {
		err = ecode.Error(ecode.ServerErr, "不存在 USDT 法币")
		return
	}

	if err = d.orm.Table("otc_advertise").
		Where("owner_id=?", userId).
		Where("coin_id=?", coin_id).
		Take(&ads).Error; err != nil {
		return
	}

	ads.IsTrade = status
	if err = d.orm.Save(&ads).Error; err != nil {
		log.Error("广告更新失败 err: %s， 当前广告可用量：%s", err.Error(), ads.RemainAmount)
		return
	}

	return
}

func (d *Dao) GetSwitchOrder(userId int64) (err error) {
	var ads model.OtcAdvertise
	coinNames := d.dataCfg.coinNames
	coin_id, ok := coinNames["USDT"]
	if !ok {
		err = ecode.Error(ecode.ServerErr, "不存在 USDT 法币")
		return
	}

	if err = d.orm.Table("member_account").
		Where("member_id=?", userId).
		Where("coin_id=?", coin_id).
		Take(&ads).Error; err != nil {
		return
	}

	return
	// ads.IsTrade = status
	// if err = d.orm.Save(&ads).Error; err != nil {
	// 	log.Error("广告更新失败 err: %s， 当前广告可用量：%s", err.Error(), ads.RemainAmount)
	// 	return
	// }

	// return
}
