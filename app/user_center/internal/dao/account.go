package dao

import (
	"encoding/json"
	"fmt"
	"git.digittraders.com/exchange/api"
	"git.digittraders.com/exchange/internal/model"
	"git.digittraders.com/exchange/paramers"
	"github.com/go-kratos/kratos/pkg/ecode"
	"github.com/go-kratos/kratos/pkg/log"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

/* 处理账户钱包 */

// 收入
func (d *Dao) IncomeCoin(userId, coinId, tread_type int64, amount float64) (account model.MemberAccount, err error) {
	// 获取上次交易记录
	// transaction := d.LastTransaction(userId, coinId)

	// 获取钱包账户
	account = d.Balance(userId, coinId)
	if account.ID == 0 {
		account, err = d.CreateAccount(userId, coinId)
		if err != nil {
			return
		}
	}

	// 比对上次交易记录 资产明细
	// if transaction

	account.RemainAmount += amount
	account.Balance = account.RemainAmount + amount
	account.UpdateAt = time.Now()
	ver := account.Version
	account.Version++
	err = d.orm.Table("member_account").
		Where("version=?", ver).
		Save(&account).Error
	return
}

// 解冻 资产 (资产回退)
func (d *Dao) Unfreeze(userId, coinId int64, amount float64) (err error) {
	var times int
again:
	balance := d.Balance(userId, coinId)
	if balance.DealAmount < amount {
		msg := fmt.Sprintf("当前冻结资产 %f 不满足 解冻数量 %f ", balance.DealAmount, amount)
		log.Error(msg)
		err = ecode.Error(ecode.ServerErr, msg)
		return
	}
	if balance.Status == 2 {
		msg := fmt.Sprintf("当前账户已冻结  userid:%d ", balance.ID)
		log.Error(msg)
		err = ecode.Error(ecode.ServerErr, msg)
		return
	}

	balance.DealAmount -= amount
	balance.RemainAmount += amount
	ver := balance.Version
	balance.Version++

	err = d.orm.Table("member_account").
		Where("version=?", ver).
		Save(&balance).Error
	if err != nil && times < 3 {
		// 重试 3 次 资产
		times++
		time.Sleep(time.Second)
		goto again
	}
	return
}

// 开始交易 冻结部分币
func (d *Dao) freeze(userId, coinId int64, amount float64) (balance model.MemberAccount, err error) {
	// 冻结部分币
	balance = d.Balance(userId, coinId)
	if balance.ID == 0 {
		balance, err = d.CreateAccount(userId, coinId)
		if err != nil {
			return
		}
	}
	if balance.RemainAmount < amount {
		log.Info("该币种可用余额不足  当前余额 %f ; 下单量：%f", balance.RemainAmount, amount)
		err = ecode.Error(ecode.ServerErr, "该币种可用余额不足 ")
		return
	}
	// 冻结 币
	balance.RemainAmount -= amount
	balance.DealAmount += amount

	// 乐观锁
	ver := balance.Version
	balance.Version++
	balance.UpdateAt = time.Now()
	err = d.orm.Table("member_account").
		Where("member_id=?", userId).
		Where("coin_id=?", coinId).
		Where("version=?", ver).
		Save(&balance).Error
	if err != nil {
		log.Info(" 冻结 币钱包失败：%v", err.Error())
		err = ecode.Error(ecode.ServerErr, "冻结 币失败 :"+err.Error())
		return
	}
	return
}

// 转账 ：扣款币 支出
func (d *Dao) Disburse(userId, coinId, tread_type int64, amount float64) (balance model.MemberAccount, err error) {
	// 冻结部分币
	balance = d.Balance(userId, coinId)
	if balance.ID == 0 {
		balance, err = d.CreateAccount(userId, coinId)
		if err != nil {
			return
		}
	}
	if balance.RemainAmount < amount {
		log.Info("该币种可用余额不足  当前余额 %f ; 下单量：%f", balance.RemainAmount, amount)
		err = ecode.Error(ecode.ServerErr, "该币种可用余额不足 ")
		return
	}
	// 扣 币
	balance.RemainAmount -= amount
	balance.Balance -= amount

	// 乐观锁
	ver := balance.Version
	balance.Version++
	balance.UpdateAt = time.Now()
	err = d.orm.Table("member_account").
		Where("member_id=?", userId).
		Where("coin_id=?", coinId).
		Where("version=?", ver).
		Save(&balance).Error
	if err != nil {
		log.Info("扣款 币钱包失败：%v", err.Error())
		err = ecode.Error(ecode.ServerErr, "扣款 币失败 :"+err.Error())
	}
	return
}

// 交易成功 扣款支出
func (d *Dao) DecreateCoin(userId, coinId, tread_type int64, amount float64) (account model.MemberAccount, err error) {
	// 获取上次交易记录
	// transaction := d.LastTransaction(userId, coinId)

	// 获取钱包账户
	account = d.Balance(userId, coinId)
	if account.ID == 0 {
		account, err = d.CreateAccount(userId, coinId)
		if err != nil {
			return
		}
	}

	// 比对上次交易记录 资产明细
	// if transaction

	if account.DealAmount < amount {
		msg := fmt.Sprintf("当前冻结资产 %f 不满足 解冻数量 %f ", account.DealAmount, amount)
		log.Error(msg)
		err = ecode.Error(ecode.ServerErr, msg)
		return
	}

	// 扣除总资产  和  可用资产
	account.DealAmount -= amount
	account.Balance -= amount
	account.UpdateAt = time.Now()
	ver := account.Version
	account.Version++

	err = d.orm.Table("member_account").
		Where("version=?", ver).
		Save(&account).Error
	if err != nil {
		return
	}
	return
}

// 增加流水记录
func (d *Dao) AddFlowRecord(parms *model.MemberTransaction) {
	d.orm.Table("member_transaction").Create(&model.MemberTransaction{
		CoinID:        parms.CoinID,
		CreateTime:    time.Now(),
		Amount:        parms.Amount,
		HistoryAmount: parms.HistoryAmount,
		Type:          parms.Type,
		MemberID:      parms.MemberID,
		Fee:           parms.Fee,
		RealFee:       parms.RealFee,
		AccountId:     parms.AccountId,
		Remark:        parms.Remark,
	})
}

// 钱包回调
func (d *Dao) CallBack() {

}

// 创建账户
func (d *Dao) CreateAccount(userId, coinId int64) (account model.MemberAccount, err error) {

	// var member model.Member
	// d.orm.Table("member").
	// 	Where("id=?", userId).
	// 	First(&member)
	//
	// if member.ID == 0 {
	// 	log.Info("用户不存在")
	// 	return
	// }
	// // 获取钱包账户
	// d.orm.Table("member_account").
	// 	Where("member_id=?", userId).
	// 	Where("coin_id=?", coinId).
	// 	First(&account)
	// if account != nil {
	// 	log.Info("账户已存在")
	// 	return
	// }

	account = model.MemberAccount{
		MemberID: userId,
		CreateAt: time.Now(),
		UpdateAt: time.Now(),
		CoinID:   coinId,
		Status:   1,
		Fee:      0,
		Balance:  0,
		Version:  1,
	}
	if err = d.orm.Table("member_account").Create(&account).Error; err != nil {
		if strings.Index(err.Error(), "member_id") != -1 {
			err = ecode.Error(ecode.ServerErr, "钱包账户已存在")
			return
		}
		return

	}
	return
}

// Balance
func (d *Dao) BalanceList(userId int64) (list []*api.Balance) {
	var accountList []model.MemberAccount
	d.orm.Table("member_account").Where("member_id=?", userId).Find(&accountList)
	coinList := d.dataCfg.coinCfg
	mapAccount := make(map[int64]string)
	for _, v := range accountList {
		aa := strconv.FormatFloat(v.RemainAmount, 'f', -1, 64)
		mapAccount[v.CoinID] = aa
	}
	for _, v := range coinList {
		temp := &api.Balance{Balance: "0", Currency: v.Symbol}
		if _, ok := mapAccount[v.ID]; ok {
			temp.Balance = mapAccount[v.ID]

			println(temp.Balance, v.Symbol)
		}
		list = append(list, temp)
	}
	return
}

func (d *Dao) GetRmbBalance() string {

	return "1000"
}

// 用户 单个币种资产
func (d *Dao) Balance(userId, coinId int64) (account model.MemberAccount) {
	d.orm.Table("member_account").
		Where("member_id=?", userId).
		Where("coin_id=?", coinId).
		First(&account)
	return
}

// 用户 上次交易记录
func (d *Dao) LastTransaction(userId, coinId int64) (record model.MemberTransaction) {
	d.orm.Table("member_transaction").Where("member_id=?", userId).
		Where("coin_id=?", coinId).
		Last(&record)
	return
}

func (d *Dao) DealHistory(req *api.DealHistoryReq, userId int64) (list []*model.MemberTransaction, err error) {
	query := d.orm.Table("member_transaction")

	query = query.Where("member_id=?", userId)

	if req.GetType() > 0 {
		query = query.Where("type=?", req.Type)
	}

	if req.GetCoinId() > 0 {
		query = query.Where("coin_id=?", req.CoinId)
	}

	if req.GetStartDealTime() > 0 {
		dealTime := time.Unix(req.StartDealTime, 0).Format("2006-01-02 15:04:05")
		query = query.Where("create_time>?", dealTime)
	}
	if req.GetEndDealTime() > 0 {
		dealTime := time.Unix(req.EndDealTime, 0).Format("2006-01-02 15:04:05")
		query = query.Where("create_time<?", dealTime)
	}

	// 下一页
	if req.GetLastId() > 0 {
		query = query.Where("id <= ?", req.LastId)

	}

	err = query.Order("id desc").Limit(50).Find(&list).Error

	return
}

// 用户充提币地址
func (d *Dao) UserWithdrawal(userId int64, req *api.GetWithdrawalReq) (list []*model.WalletAddress) {
	d.orm.Table("wallet_address").
		Where("member_id=?", userId).
		Where("type=?", req.Type).
		Find(&list)
	return
}

type RespData struct {
	Code    int64  `json:"code"`
	Data    string `json:"data"`
	Message string `json:"message"`
}

// 用户添加充币地址
func (d *Dao) AddDeposit(req *api.DepositMs, userId int64) (wallet_id int64, add_ress string, err error) {
	var wallet model.WalletAddress
	d.orm.Table("wallet_address").
		Where("member_id=?", userId).
		Where("coin_id=?", req.CoinId).
		Where("type=1").
		Take(&wallet)
	// 已经存在该币地址
	if wallet.ID > 0 {
		add_ress = wallet.Address
		wallet_id = wallet.ID
		return
	}
	url := fmt.Sprintf("http://192.168.0.125:9526/rpc/address/%d", userId)
	resp, err := http.Get(url)
	if err != nil {
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	response := &RespData{}
	json.Unmarshal(body, response)
	if err != nil {
		return
	}

	if response.Code == -1 {
		err = ecode.Error(ecode.ServerErr, "ETH 创建提币地址失败")
		return
	}

	url = fmt.Sprintf("http://192.168.0.125:9527/rpc/address/%d", userId)
	resp, err = http.Get(url)
	if err != nil {
		return
	}

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	response = &RespData{}
	json.Unmarshal(body, response)
	if err != nil {
		return
	}

	if response.Code == -1 {
		err = ecode.Error(ecode.ServerErr, "USDT创建提币地址失败")
		return
	}

	if wallet.ID == 0 {
		wallet.Address = response.Data
		wallet.CoinID = req.CoinId
		wallet.CreateTime = time.Now()
		wallet.ProviderID = req.ProviderId
		wallet.MemberID = userId
		wallet.Type = paramers.WALLET_ADDRESS_DEPOSIT
		d.orm.Table("wallet_address").Create(&wallet)
	}
	add_ress = response.Data
	wallet_id = wallet.ID
	return
}

// 用户添加提币地址
func (d *Dao) AddWithdrawal(req *api.DepositMs, userId int64) (err error) {
	var wallet model.WalletAddress
	wallet.Address = req.Address
	wallet.CoinID = req.CoinId
	wallet.CreateTime = time.Now()
	wallet.ProviderID = req.ProviderId
	wallet.MemberID = userId
	wallet.Type = paramers.WALLET_ADDRESS_WITHDRAWAL
	err = d.orm.Table("wallet_address").Create(&wallet).Error
	return
}

// 提币操作
func (d *Dao) Withdrawal(req *api.WithdrawalReq, userId int64) (err error) {

	remark, amount, address := "", "", ""
	url := fmt.Sprintf("http://192.168.0.125:9526/rpc/withdraw?remark=%s&amount=%s&address=%s", remark, amount, address)
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	//
	defer resp.Body.Close()
	// body, err := ioutil.ReadAll(resp.Body)
	// response := &RespData{}
	// json.Unmarshal(body, response)
	// if err != nil {
	// 	return
	// }
	//
	// if response.Code == -1 {
	// 	err = ecode.Error(ecode.ServerErr, "ETH 创建提币地址失败")
	// 	return
	// }
	// /rpc/withdraw
	var wallet model.WalletAddress

	err = d.orm.Table("wallet_address").
		Where("member_id=?", userId).
		Where("address=?", req.Address).
		Where("coin_id=?", req.CoinId).
		Where("provider_id=?", req.ProviderId).
		Take(&wallet).Error
	if err != nil {
		err = ecode.Error(ecode.ServerErr, "提币地址错误，不存在")
		return
	}

	return
}
