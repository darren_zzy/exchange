package dao

import "fmt"

// redis member
func RKTokenUsers(userId string) string {
	return fmt.Sprintf("TOKENS:{%s}", userId)
}

// RKUser 用户信息，String结构
func RKUser(userId int64) string {
	return fmt.Sprintf("U:{%d}", userId)
}

// RKTradingUser 用户交易信息，】
func RKTradingUser(userId int64) string {
	return fmt.Sprintf("UserTrading:{%d}", userId)
}
