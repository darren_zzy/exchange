package dao

import (
	"git.digittraders.com/exchange/internal/model"
	"git.digittraders.com/exchange/pkg/lib"
	"github.com/go-kratos/kratos/pkg/cache/redis"
	"github.com/go-kratos/kratos/pkg/ecode"
	"github.com/go-kratos/kratos/pkg/log"
	bm "github.com/go-kratos/kratos/pkg/net/http/blademaster"
	"golang.org/x/net/context"
	"strconv"
	"strings"
)

// 校验签名
func (d *Dao) AuthUser(c *bm.Context) bool {
	userId := c.Request.Header.Get("userID")
	key := RKTokenUsers(userId)
	token := c.Request.Header.Get("token")

	if authorize, err := redis.String(d.redis.Do(c, "get", key)); err == nil && authorize != "" {
		remoteIps := strings.Split(c.Request.RemoteAddr, ":")
		// 	加密校验是否正确
		if auth2 := lib.Authorize(token, "", remoteIps[0], 10); auth2 == authorize {
			// 认证成功做延时
			d.redis.Do(c, "EXPIRE", key, 360000)
			// log.Info("userId :  Authentication success do expire 3600s", userId)
			return true
		} else {
			log.Error("Authentication error, addr：%s, user: %s, makeUser: %s,key: %s", remoteIps[0], userId, authorize, key)
		}
	}
	return false
}

// 通过请求头中 userId 和 remoteAddr
func (d *Dao) SetAuthorize(c context.Context, member model.Member) error {
	RemoteAddr := c.Value("remote_addr")
	remote := lib.GetRemoteIp(RemoteAddr.(string))
	authorize := lib.Authorize(member.Token, "", remote, 10)
	key := RKTokenUsers(strconv.FormatInt(member.ID, 10))
	if _, err := d.redis.Do(c, "SET", key, authorize, "EX", 360000); err != nil {
		return ecode.Error(ecode.AccessDenied, " 鉴权失败： :"+err.Error())

	}
	log.Info(" 鉴权成功： %s,%s", key, remote)
	return nil
}
