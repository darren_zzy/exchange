package dao

import (
	"context"
	"git.digittraders.com/exchange/internal/model"
	"github.com/go-kratos/kratos/pkg/cache/redis"
	"github.com/go-kratos/kratos/pkg/conf/paladin"
	"github.com/go-kratos/kratos/pkg/database/sql"
	"github.com/go-kratos/kratos/pkg/sync/pipeline/fanout"
	xtime "github.com/go-kratos/kratos/pkg/time"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	"sync"
	"time"
)

// var Provider = wire.NewSet(New, NewDB, NewRedis, NewMC, NewORM)
var Provider = wire.NewSet(New, NewRedis, NewORM)

// dao dao.
type Dao struct {
	dbCfg      sql.Config
	dbCt       paladin.TOML
	redis      *redis.Redis
	orm        *gorm.DB
	cache      *fanout.Fanout
	demoExpire int32
	dataCfg    *dataCfg
}

type dataCfg struct {
	cfgLocker  sync.RWMutex
	coinCfg    map[int64]*model.Coin    // 币基本数据配置
	otcCoinCfg map[int64]*model.OtcCoin // 法币基本数据配置
	coinNames  map[string]int64         // 币基本数据配置
}

// New new a dao and return.
func New(r *redis.Redis, orm *gorm.DB) (d Dao, cf func(), err error) {
	var cfg struct {
		DemoExpire xtime.Duration
	}
	if err = paladin.Get("application.toml").UnmarshalTOML(&cfg); err != nil {
		return
	}
	d = Dao{
		redis:      r,
		orm:        orm,
		cache:      fanout.New("cache"),
		demoExpire: int32(time.Duration(cfg.DemoExpire) / time.Second),
	}
	cf = d.Close
	d.loadCoinCfg()
	return
}

// ----------------启动项目 读取货币信息----------------
func (d *Dao) loadCoinCfg() {
	d.dataCfg = new(dataCfg)
	coins, err := d.LoadCoins()
	if err != nil {
		panic(err.Error())
	}
	d.dataCfg.coinCfg = make(map[int64]*model.Coin)
	d.dataCfg.otcCoinCfg = make(map[int64]*model.OtcCoin)
	d.dataCfg.coinNames = make(map[string]int64)

	otc_coins, err := d.LoadOtcCoins()

	d.dataCfg.cfgLocker.RLock()

	for _, v := range otc_coins {
		d.dataCfg.otcCoinCfg[v.CoinID] = v
	}

	for _, v := range coins {
		d.dataCfg.coinCfg[v.ID] = v
		d.dataCfg.coinNames[v.Symbol] = v.ID
	}
	d.dataCfg.cfgLocker.RUnlock()

}

func (d *Dao) LoadCoins() (coins []*model.Coin, err error) {
	err = d.orm.Table("coin").
		Where("status=?", 1).
		// Order("sort desc").
		Find(&coins).Error
	return
}

func (d *Dao) LoadOtcCoins() (coins []*model.OtcCoin, err error) {
	err = d.orm.Table("otc_coin").
		Where("status=?", 1).
		Find(&coins).Error
	return
}

// Close close the resource.
func (d *Dao) Close() {
	d.cache.Close()
}

// Ping ping the resource.
func (d *Dao) Ping(ctx context.Context) (err error) {
	return nil
}
